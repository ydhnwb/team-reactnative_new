import React from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';
import { Card } from "react-native-paper";
import * as res from './../values/colors';
import * as utils from './../utils/utils';
import * as urls from './../service/urls';

export const CommentItem = ({ comment }) => {
    const processDate = (d) => {
        const dayDiffer = utils.dayDiffer(d)
        if (dayDiffer == 0) {
            return "Today"
        } else if (dayDiffer === -1) {
            return "Yesterday"
        }
        return `${Math.abs(dayDiffer)} days ago`
    }

    return (
        <Card style={styles.root}>
            <View style={styles.header}>
                <Image style={styles.userImage} source={{ uri: comment.User === undefined ? urls.DEF_USER_IMAGE : comment.User.photo }} />
                <View style={styles.user}>
                    <Text style={styles.textAmount}>{comment.User.name}</Text>
                    <Text style={{ color: 'gray', fontFamily: "Nunito-Regular", }}>{processDate(comment.date)}</Text>
                </View>
            </View>
            <Text style={styles.textMessage}>{comment.content}</Text>
        </Card>
    )
}
const styles = StyleSheet.create({
    root: {
        margin: 4,
        padding: 8,
    },
    header: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "flex-start",
    },
    userImage: {
        width: 40,
        height: 40,
        borderRadius: 6,
    },
    user: {
        marginStart: 16
    },
    textAmount: {
        fontFamily: "Nunito-Regular",
        fontWeight: "700",
        color: res.PRIMARY_COLOR
    },
    textTime: {
        color: 'gray',
        fontSize: 12
    },
    textMessage: {
        fontFamily: "Nunito-Regular",
        paddingVertical: 16
    }
})