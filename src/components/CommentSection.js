import React, { useState } from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native';
import { Button } from 'react-native-paper';
import { TextInput } from 'react-native-paper';
import { useSelector } from 'react-redux';
import { generalStyles } from '../values/styles';
import * as res from './../values/colors';
import { CommentItem } from './CommentItem';
import { PrimaryButton } from './PrimaryButton';
import * as CampaignService from './../service/CampaignService';



export const CommentSection = ({ refreshComment, campaignId, comments = [], onTap = () => { } }) => {
    const [isLoading, setIsLoading] = useState(false)
    const userState = useSelector(state => state.userReducer)
    const [payload, setPayload] = useState({
        UserId: userState.user?.id,
        CampaignId: campaignId,
        content: ''
    })

    const postComment = async () => {
        setIsLoading(true)
        const res = await CampaignService.addComment(userState.token, campaignId, payload)
        setIsLoading(false)
        console.log(res)
        if (res.status === 201) {
            setPayload({ ...payload, content: '' })
            refreshComment()
        } else {
            console.log("Failed to post your comment")
        }
    }

    return (

        <View style={styles.borderContainer}>
            <Text style={styles.titleText}>Comments</Text>
            {
                userState.user !== null ?
                    <View>
                        <TextInput value={payload.content} onChangeText={(t) => setPayload({ ...payload, content: t })} multiline={true} numberOfLines={4} theme={generalStyles.input} label="Give your support" />
                        <Button disabled={isLoading} color={res.PRIMARY_BUTTON_COLOR} mode="contained" style={{ marginTop: 16, alignSelf: 'flex-end' }} onPress={postComment}>
                            POST
                        </Button>
                    </View>
                    :
                    null
            }


            <FlatList
                style={{ marginTop: 16 }}
                scrollEnabled={false}
                keyExtractor={(item) => item.id.toString()}
                data={
                    comments.length > 6 ? comments.slice(0, 4) : comments
                }
                renderItem={({ item }) => {
                    return (
                        <CommentItem comment={item} />
                    )
                }}
            />

            {
                comments.length === 0 ? 
                <Text>No comments for now</Text> :
                    <Button mode="outlined" style={{ alignSelf: "center" }} onPress={onTap}>
                        SHOW ALL
                </Button>
            }

        </View>
    )
}

const styles = StyleSheet.create({
    titleText: {
        fontFamily: "Nunito-Regular",
        fontWeight: "bold",
        alignSelf: "stretch",
        fontSize: 16,
        color: 'black'
    },
    borderContainer: {
        padding: 16,
        borderRadius: 4,
        justifyContent: 'center',
        borderColor: 'gray',
        borderWidth: 0.2
    },
    content: {
        flexDirection: 'row',
        justifyContent: 'center'
    },
})