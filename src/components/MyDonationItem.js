import React from 'react';
import { Text, StyleSheet } from 'react-native';
import { Avatar, Button, Card, Title, Paragraph } from 'react-native-paper';
import * as res from './../values/colors';
import * as utils from './../utils/utils';

export const MyDonationItem = ({ donation }) => {
    const timeDiffer = (date) => {
        const dayDiffer = utils.dayDiffer2(date)
        if (dayDiffer == 0) {
            return "Today"
        }
        return `${Math.abs(dayDiffer)} days ago`
    }

    const showAmount = (amount) => {
        if(amount == null){
            return "~"
        }
        const inCurrencyFormat = utils.formatRupiah(amount.toString(), "IDR ")
        return inCurrencyFormat
    }

    return (
        <Card style={styles.card}>
            <Text style={styles.dateText}>{timeDiffer(donation.createdAt)}</Text>
            <Text style={styles.titleText}>{donation.Campaign.title}</Text>
            <Text style={styles.donationAmountText}>{showAmount(donation.amount)}</Text>
            <Text style={styles.subtitleText}>{donation.comment}</Text>
        </Card>
    )
}

const styles = StyleSheet.create({
    card: {
        marginVertical: 4,
        marginHorizontal: 8,
    },
    dateText: {
        color: 'gray',
        fontFamily: "Nunito-Regular",
        fontSize: 12,
        marginEnd: 16,
        marginTop: 16,
        alignSelf: 'flex-end'
    },
    titleText: {
        fontFamily: "Nunito-Regular",
        fontWeight: "bold",
        marginHorizontal: 16,
        alignSelf: "stretch",
        fontSize: 16,
        textDecorationLine: 'underline',
        color: 'black'
    },
    donationAmountText: {
        fontFamily: "Nunito-Regular",
        fontWeight: "700",
        marginHorizontal: 16,
        color: res.PRIMARY_COLOR,
        fontSize: 20
    },
    subtitleText: {
        fontFamily: "Nunito-Regular",
        color: 'gray',
        marginTop: 8,
        marginBottom: 16,
        marginHorizontal: 16
    }
})
