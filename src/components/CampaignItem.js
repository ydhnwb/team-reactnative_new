import React from 'react';
import { StyleSheet, View, Text, Dimensions, Image } from 'react-native';
import { Card, Title, Paragraph, ProgressBar } from 'react-native-paper';
import { OutlinedCategory } from './OutlinedCategory';
import * as res from '../values/colors';
import * as utils from './../utils/utils';
import HTMLView from 'react-native-htmlview';

const width = Dimensions.get('window').width


export const CampaignItem = ({ small = false, onTap, campaign = {
    id: 0,
    header_img: "",
    title: "",
    story: "",
    Category: {
        name: ""
    }
} }) => {

    return (
        <Card backgroundColor="blue" onPress={() => onTap(campaign)} style={small ? styles.cardSmall : styles.card}>
            <View style={{
                borderRadius: 8
            }}>
                <Image style={small ? styles.small : styles.normal} source={{ uri: campaign.header_img }} />
            </View>

            <View style={styles.category}>
                <OutlinedCategory category={campaign.Category} />
            </View>
            <Card.Content>
                <Title numberOfLines={2} style={styles.title}>{campaign.title}</Title>

                <ProgressBar style={styles.bar} color={res.PRIMARY_COLOR} progress={utils.getProgress(campaign.raised === undefined ? 0 : campaign.raised, campaign.goal === undefined ? 0 : campaign.goal)} />
                <View style={styles.info}>
                    <View style={styles.infoDetailStart}>
                        <Text style={small ? styles.helperTextSmall : styles.helperTextNormal}>Raised</Text>
                        <Text style={small ? styles.infoTextRaisedSmall : styles.infoTextRaisedNormal}>{campaign.raised === undefined || campaign.raised === null ? 0 : utils.formatRupiah(campaign.raised.toString(), "IDR ")}</Text>
                    </View>
                    <View style={styles.infoDetailEnd}>
                        <Text style={small ? styles.helperTextSmall : styles.helperTextNormal}>Goal</Text>
                        <Text style={small ? styles.infoTextGoalSmall : styles.infoTextGoalNormal}>{campaign.goal === undefined || campaign.goal === null ? 0 : utils.formatRupiah(campaign.goal.toString(), "IDR ")}</Text>
                    </View>

                </View>
            </Card.Content>
        </Card>
    )
}

const styles = StyleSheet.create({
    card: {
        borderBottomStartRadius: 8,
        borderBottomEndRadius: 8,
        borderTopStartRadius: 8,
        borderTopEndRadius: 8,
        elevation: 4,
        marginVertical: 4,
        marginHorizontal: 8
    },
    cardSmall: {
        borderBottomStartRadius: 8,
        borderBottomEndRadius: 8,
        borderTopStartRadius: 8,
        borderTopEndRadius: 8,
        elevation: 4,
        margin: 8,
        width: width * 0.8
    },
    category: {
        marginTop: 12,
        marginHorizontal: 14,
        fontFamily: "Nunito-Regular",

    },
    title: {
        marginTop: 8,
        lineHeight: 20,
        fontSize: 17,
        fontFamily: "Nunito-Regular",
        fontWeight: "bold",
        textDecorationLine: 'underline'
    },
    bar: {
        marginTop: 8,
        borderRadius: 8,
        height: 6
    },
    info: {
        marginTop: 8,
        justifyContent: "space-between",
        flexDirection: "row"
    },
    infoDetailStart: {
        alignItems: "flex-start",
        flexDirection: "column",
    },
    infoDetailEnd: {
        alignItems: "flex-end",
        flexDirection: "column",
    },
    infoTextRaisedNormal: {
        color: res.PRIMARY_COLOR,
        fontFamily: "Nunito-Regular",
        fontWeight: "bold"
    },
    infoTextRaisedSmall: {
        color: res.PRIMARY_COLOR,
        fontSize: 12,
        fontFamily: "Nunito-Regular",
        fontWeight: "bold"

    },
    infoTextGoalNormal: {
        fontFamily: "Nunito-Regular",
        fontWeight: "bold",
    },
    infoTextGoalSmall: {
        fontFamily: "Nunito-Regular",
        fontSize: 12
    },
    normal: {
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
        height: 155,
    },
    small: {
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
        height: 150,
    },
    helperTextNormal: {
        fontFamily: "Nunito-Regular",
        color: 'gray'

    },
    helperTextSmall: {
        fontFamily: "Nunito-Regular",
        fontSize: 12,
        color: 'gray'
    }

})