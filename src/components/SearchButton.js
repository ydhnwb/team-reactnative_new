import React from 'react';
import { StyleSheet, View, } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Feather from 'react-native-vector-icons/Feather';
import * as res from './../values/colors';

export const SearchButton = ({ onTap }) => {
    return (
        <TouchableOpacity onPress={onTap}>
            <View style={styles.root}>
                <Feather name="search" size={32} color={res.PRIMARY_COLOR}/>
            </View>
        </TouchableOpacity>

    )
}

const styles = StyleSheet.create({
    root: {
        justifyContent: 'center',
        borderWidth: 0.5,
        padding: 8,
        margin: 4,
        borderRadius: 4,
        borderColor: res.PRIMARY_COLOR
    }
})