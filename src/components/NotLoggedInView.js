import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { Button } from 'react-native-paper';
import { PrimaryButton } from './../components/PrimaryButton';
import * as res from '../values/colors';

export const NotLoggedInView = ({ onLoginTap, onRegisterTap }) => {

    return (
        <View>
            <Text style={styles.title}>You are not logged in.</Text>
            <Text style={styles.subtitle}>Sign in to start your campaign or donate.</Text>
            <View style={{ flexDirection: "row" }}>
                <Button style={{ marginEnd: 8 }} color={res.PRIMARY_COLOR} mode="outlined" onPress={onRegisterTap}>Create account</Button>
                <PrimaryButton title="Sign in" onPress={onLoginTap} />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    title: {
        fontFamily: "Nunito-Regular",
        fontWeight: 'bold',
    },
    subtitle: {
        fontFamily: "Nunito-Regular",
        marginBottom: 16,
        color: 'gray'
    }
})