import React from 'react'
import { Image, StyleSheet, Text, View, Dimensions } from 'react-native'
import { Avatar, Button, Card, Title, Paragraph, ProgressBar } from 'react-native-paper'
import DataImport from '../../Config/DataImport.json';
import ServerAPI from '../pages/server/ServerAPI'

const DEVICE = Dimensions.get('window');

export default function HomeComponent({ donations }) {
    return (
        <View style={{ borderRadius: 8 }}>
            <Card style={styles.container}>
                <Card.Cover source={{ uri: donations.gambar }} style={styles.image} />
                <Card.Content style={styles.content}>
                    <Text style={styles.text}>{donations.kategori}</Text>
                    <Title style={styles.title}>{donations.title}</Title>
                    <Paragraph style={styles.paragraph}>{donations.subtitle}</Paragraph>
                    <View>
                        <ProgressBar style={{ top: 50 }} progress={0.8} color="#1d94a8" />
                    </View>
                    <View flexDirection="row">
                        <Text style={styles.Raised}>Raised</Text>
                        <Text style={styles.Goal}>Goal</Text>
                    </View>
                    <View flexDirection="row">
                        <Text style={styles.idr1}>IDR 30.000.000</Text>
                        <Text style={styles.idr2}>IDR 50.000.000</Text>
                    </View>

                </Card.Content>
            </Card>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: 250,
        height: 300,
        marginLeft: 15,
        marginRight: 5,
        bottom: 15,
        borderWidth: 5,
        borderColor: "#fff",
        borderBottomEndRadius: 8,
        borderBottomStartRadius: 8,
        borderTopEndRadius: 10,
        borderTopStartRadius: 10,
        shadowColor: '#000',
        shadowOffset: {
            width: 5,
            height: 5,
        },
        shadowOpacity: 1,
        shadowRadius: 3,
        elevation: 10,


    },

    image: {
        top: 10,
        left: -5,
        borderTopStartRadius: 8,
        borderTopEndRadius: 8,
        width: 250,
        height: 130,



    },

    text: {
        top: 18,
        left: 13,
        display: "flex",
        position: "absolute",
        flexDirection: "row",
        alignItems: "flex-start",
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 5,
        paddingRight: 5,
        color: '#A43F3C',
        borderWidth: 1,
        borderRadius: 4,
        borderColor: '#A43F3C',

    },

    title: {
        top: 50,
        lineHeight: 16,
        fontSize: 15,
    },
    paragraph: {
        top: 45,
    },

    progress: {
        backgroundColor: '#f4f4f4',
        borderRadius: 4,
        position: "relative",
        marginTop: 50,
        height: 8,
        width: "100%",
    },

    Raised: {
        top: 50,
        fontSize: 12,
        color: "#a0a0a0",
    },

    Goal: {
        top: 50,
        left: 148,
        fontSize: 12,
        color: "#a0a0a0",
    },

    idr1: {
        top: 50,
        fontWeight: "bold",
        color: "#1d94a8",
        fontSize: 12,

    },

    idr2: {
        top: 50,
        left: 45,
        fontSize: 12,
    },

    content: {
        borderRadius: 10,
    }

})
