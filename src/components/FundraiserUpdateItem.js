import React from 'react';
import { Text, View, StyleSheet, Dimensions } from 'react-native';
import HTMLView from 'react-native-htmlview';
import * as res from './../values/colors';
import * as utils from './../utils/utils';

const dim = Dimensions.get('window')

export const FundraiserUpdateItem = ({ item }) => {
    return (
        <View style={{ flexDirection: 'row', flex: 1, }}>
            <View style={{ alignItems: "center" }}>
                <View style={styles.circle} />
                <View style={styles.line} />
            </View>
            <View style={{ marginStart: 16, alignSelf: 'stretch', alignItems: 'stretch', flexDirection: 'column' }}>
                <View style={{ flexDirection: 'row', alignSelf: 'stretch', alignContent: 'stretch', alignItems: 'stretch' }}>
                    <Text style={{ fontWeight: 'bold' }}>{utils.isoDateToMoment(item.createdAt)}</Text>
                    {
                        item.ammount === 0 ? <Text style={{ marginStart: 16, color: 'gray', fontSize: 12 }}>Recipient update</Text> : <Text style={styles.withBorder}>Withdrawal</Text>
                    }

                </View>
                <View style={{ alignItems: 'stretch', paddingVertical: 8, marginEnd: 16 }}>
                    {
                        item.ammount === 0 ?
                            <HTMLView style={{
                                flex: 1,
                                borderRadius: 6,
                                padding: 8,
                                borderColor: 'gray',
                                borderWidth: 0.5,
                                fontFamily: "Nunito-Regular",
                            }} value={item.content} />
                            :
                            <View style={{
                                flex: 1,
                                borderRadius: 6,
                                padding: 8,
                                backgroundColor: res.COLOR_SILK
                            }}>
                                <Text style={{ color: 'gray', fontSize: 12, fontFamily: "Nunito-Regular", }}>Withdrawal purpose</Text>
                                <HTMLView value={item.content} />
                            </View>
                    }
                </View>


            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    circle: {
        backgroundColor: res.PRIMARY_COLOR,
        borderRadius: 32,
        width: 16,
        height: 16
    },
    line: {
        flex: 1,
        borderLeftWidth: 2,
        borderLeftColor: res.PRIMARY_COLOR,
    },
    withBorder: {
        marginStart: 16,
        color: res.COLOR_CHOPPER,
        fontSize: 12,
        fontWeight: "400",
        paddingHorizontal: 6,
        borderRadius: 4,
        borderWidth: 1,
        borderColor: res.COLOR_CHOPPER,
    },

})