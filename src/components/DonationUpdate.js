import React from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native';
import { DonationUpdateItem } from './DonationUpdateItem';
import { Button } from 'react-native-paper';


export const DonationUpdate = ({ donations = [], onTap = () => { } }) => {
    return (
        <View style={styles.borderContainer}>
            <Text style={styles.titleText}>Donations</Text>
            <FlatList
                style={{ marginTop: 16 }}
                scrollEnabled={false}
                keyExtractor={(item) => item.id.toString()}
                data={donations}
                renderItem={({ item }) => {
                    return (
                        <DonationUpdateItem item={item} />
                    )
                }}
            />

            {
                donations.length === 0 ?
                    <Text>No donations for now</Text> :
                    <Button mode="outlined" style={{ alignSelf: "center" }} onPress={onTap}>
                        SHOW ALL
            </Button>
            }


        </View>
    )
}

const styles = StyleSheet.create({
    titleText: {
        fontFamily: "Nunito-Regular",
        fontWeight: "bold",
        alignSelf: "stretch",
        fontSize: 16,
        color: 'black'
    },
    borderContainer: {
        padding: 16,
        borderRadius: 4,
        justifyContent: 'center',
        borderColor: 'gray',
        borderWidth: 0.2

    },
    content: {
        flexDirection: 'row',
        justifyContent: 'center'
    },
})