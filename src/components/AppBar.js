import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import TaliKasih from './../assets/talikasih.svg';
import { Divider, IconButton } from 'react-native-paper';
import * as res from './../values/colors';
import { useSelector } from 'react-redux';
import { TouchableOpacity } from 'react-native-gesture-handler';


export const AppBar = ({ navigation }) => {
    const authState = useSelector(state => state.userReducer)
    const onAppBarPressed = () => navigation.popToTop()
    
    return (
        <View>
            <View style={styles.appBar}>
                <View style={styles.leftContent}>
                    <TouchableOpacity  onPress={() => onAppBarPressed()}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <TaliKasih width={32} height={32} />
                            <Text style={styles.title}>
                                <Text style={styles.textBold}>TALI</Text>
                                <Text>KASIH</Text>
                            </Text>
                        </View>

                    </TouchableOpacity>

                </View>
                <View style={styles.rightContent}>
                    <IconButton
                        style={{ margin: 0 }}
                        icon="magnify"
                        color={res.PRIMARY_COLOR}
                        size={res.ICON_SIZE_SMALL}
                        onPress={() => navigation.navigate("Explore")} />
                    {
                        authState.token === null ?
                            <Text onPress={() => navigation.navigate("Login")} style={styles.titleSmall}>Login</Text>
                            :
                            null
                    }
                </View>
            </View>
            <Divider />
        </View>
    )
}

const styles = StyleSheet.create({
    appBar: {
        backgroundColor: 'white',
        paddingVertical: 8,
        alignContent: "center",
        flexDirection: "row",
        justifyContent: "space-between"
    },
    icon: {
        alignSelf: "flex-start"
    },
    leftContent: {
        marginStart: 10,
        alignItems: "center",
        flexDirection: "row",
        alignSelf: "flex-start"
    },
    rightContent: {
        marginEnd: 8,
        alignItems: "center",
        flexDirection: "row",
        alignSelf: "flex-end"
    },
    title: {
        marginStart: 8,
        fontSize: 18,
        color: res.PRIMARY_COLOR
    },
    titleSmall: {
        marginHorizontal: 16,
    },
    textBold: {
        fontWeight: "bold"
    },
})