import React, { useEffect } from 'react';
import { StyleSheet, View, Text, Image, Dimensions } from 'react-native';
import { Card, Title, Paragraph, ProgressBar } from 'react-native-paper';
import * as res from '../values/colors';
import * as utils from '../utils/utils';

const dim = Dimensions.get('window')


export const FundraiseInfo = ({ campaign = {} }) => {

    return (

        campaign === {} ? null :
            <View style={styles.borderContainer}>
                <Text style={styles.textRaised}>{utils.currencyFormat(campaign.raised === undefined ? 0 : campaign.raised)}</Text>
                <Text style={styles.textRaisedInfo}>Raised from {campaign.goal === undefined ? 0 : utils.formatRupiah(campaign.goal.toString(), "IDR ")} goal</Text>
                <ProgressBar style={styles.bar} color={res.PRIMARY_COLOR} progress={utils.getProgress(campaign.raised === undefined ? 0 : campaign.raised, campaign.goal === undefined ? 0 : campaign.goal)} />
                {
                    campaign.User === undefined ? null
                        :
                        <View style={styles.content}>
                            <Image style={styles.userImage} source={{ uri: campaign.User === undefined ? "" : campaign.User.photo }} />
                            <View style={styles.userInfo}>
                                <Text style={styles.userName}>{campaign.User.name}</Text>
                                <Text style={styles.userRole}>Fundraiser</Text>
                            </View>
                        </View>
                }

                <View style={styles.userTabContainer}>
                    <Card>
                        <View style={{ alignItems: "center", padding: 16, width: dim.width * 0.25 }}>
                            <Text style={styles.textNumber}>{campaign.donationCount}</Text>
                            <Text style={styles.textAux}>Donations</Text>
                        </View>
                    </Card>

                    <Card>
                        <View style={{ alignItems: "center", padding: 16, width: dim.width * 0.25 }}>
                            <Text style={styles.textNumber}>{campaign.shareCount}</Text>
                            <Text style={styles.textAux}>Share</Text>
                        </View>
                    </Card>

                    <Card>
                        <View style={{ alignItems: "center", padding: 16, width: dim.width * 0.25 }}>
                            <Text style={styles.textNumber}>{
                                campaign.due_date === null ? "~" : Object.keys(campaign).length === 0 ? null : utils.dayDiffer(campaign.due_date)
                            }</Text>
                            <Text style={styles.textAux}>{
                                campaign.due_date === null ? "No due date" : "Days left"
                            }</Text>
                        </View>
                    </Card>
                </View>
            </View>


    )
}

const styles = StyleSheet.create({
    borderContainer: {
        padding: 16,
        borderRadius: 4,
        justifyContent: 'center',
        borderColor: 'gray',
        borderWidth: 0.2

    },
    content: {
        flexDirection: 'row',
        justifyContent: 'center'
    },
    textRaised: {
        fontFamily: "Nunito-Regular",
        fontWeight: "bold",
        fontSize: 22,
        color: res.COLOR_ROSE
    },
    textRaisedInfo: {
        fontFamily: "Nunito-Regular",
        color: 'gray',
        fontSize: 14,
    },
    bar: {
        marginVertical: 16,
        borderRadius: 4,
        height: 6
    },
    userImage: {
        borderRadius: 6,
        width: 32,
        height: 32,
    },
    userInfo: {
        flex: 1,
        flexDirection: "column",
        marginStart: 16,
    },
    userName: {
        fontFamily: "Nunito-Regular",
        fontWeight: "bold",
        fontSize: 16
    },
    userRole: {
        fontFamily: "Nunito-Regular",
        color: 'gray',
        fontSize: 14
    },
    userTabContainer: {
        marginVertical: 16,
        justifyContent: "space-around",
        alignSelf: 'stretch',
        flexDirection: 'row'
    },
    textNumber: {
        fontFamily: "Nunito-Regular",
        color: res.PRIMARY_COLOR,
        fontSize: 20
    },
    textAux: {
        fontFamily: "Nunito-Regular",
        color: 'gray',
        fontSize: 12

    }
})