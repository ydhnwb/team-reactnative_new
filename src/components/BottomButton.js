import React from 'react';
import { View, Text, StyleSheet, } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

export const BottomButton = ({ title, onTap }) => {
    return (
        <View style={styles.body}>
            <TouchableOpacity onPress={onTap}>
                <Text style={styles.titleText}>{title}</Text>
            </TouchableOpacity>
        </View>

    )
}

const styles = StyleSheet.create({
    body: {
        padding: 8
    },
    titleText: {
        fontFamily: "Nunito-Regular",
        fontSize: 16,
        color: 'white'
    }
})