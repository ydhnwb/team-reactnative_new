import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import * as res from './../values/colors';
import * as utils from './../utils/utils';

export const TransferForm = ({ user = null }) => {
    return (
        <View style={styles.border}>
            <Text style={{ fontFamily: "Nunito-Regular", fontWeight: "bold", color: res.PRIMARY_COLOR }}>Transfer to</Text>
            <Text style={styles.helper}>Account number</Text>
            <View style={styles.content}>
                <Text style={styles.contentText}>{user == null ? "" : user.bank_account}</Text>
                <TouchableOpacity>
                    <Text style={styles.contentText}>COPY</Text>
                </TouchableOpacity>
            </View>
            <Text style={styles.helper}>Account holder name</Text>
            <View style={styles.content}>
                <Text style={styles.contentText}>{user == null ? "" : user.name}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    border: {
        backgroundColor: res.COLOR_PLATINUM,
        marginTop: 16,
        padding: 16,
        borderRadius: 4,
        justifyContent: 'center',
    },
    helper: {
        fontFamily: "Nunito-Regular",
        color: 'gray',
        marginVertical: 8
    },
    content: {
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    contentText: {
        fontFamily: "Nunito-Regular",
        fontWeight: 'bold',
        fontSize: 16
    }
})