import React from 'react'
import { View, Dimensions } from 'react-native'
import Health from '../assets/Health.svg'
import Edu from './../assets/Eduation.svg';
import Carousel from 'react-native-snap-carousel';


const { width, height } = Dimensions.get('window');


export default function CarouselComponent() {
    //its a trick to create 3 array
    const entries = ["", "", ""]

    const _renderItemCarousel = ({ item, index }) => {
        return (
            <View style={{ alignSelf: 'center', width: '100%', margin: 8 }}>
                {
                    index === 0 ? <Health width='100%' height={180} />
                        : index === 1 ? <Edu width='100%' height={180} />
                            : <Health width='100%' height={180} />
                }
            </View>
        );
    }
    return (
        <View>
            <Carousel
                autoplay={true}
                data={entries}
                renderItem={_renderItemCarousel}
                sliderWidth={width}
                itemWidth={width}
            />
        </View>
    )
}
