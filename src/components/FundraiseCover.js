import React from 'react';
import { View, Dimensions, Image, Text, StyleSheet } from 'react-native';

export const FundraiseCover = ({ header_img_url }) => {
    return (
        <Image source={{ uri: header_img_url }} style={{ flex: 1, height: 200, overflow: 'hidden' }}/>
    )
}


