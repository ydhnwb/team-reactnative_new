import React from 'react';
import { View } from 'react-native';
import { TextInput, Button, Checkbox } from 'react-native-paper';
import { generalStyles } from './../values/styles';


export const CardForm = () => {
    return(
        <View style={{ alignContent: 'center'}}>
            <TextInput theme={generalStyles.input} placeholder="Card number" style={{marginTop: 16}}/>
            <View style={{ flexDirection: 'row', flex: 1  }}>
                <TextInput theme={generalStyles.input} placeholder="Expiry date" style={{ flexGrow: 2,marginVertical: 4, marginHorizontal: 4}}/>
                <TextInput theme={generalStyles.input} placeholder="CVV" style={{ flexGrow: 1, marginHorizontal: 4, marginVertical: 4,}}/>
            </View>
        </View>
    )
}