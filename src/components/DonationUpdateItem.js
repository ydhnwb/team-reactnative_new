import React from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';
import { Card } from "react-native-paper";
import * as res from './../values/colors';
import * as utils from './../utils/utils';
import * as urls from './../service/urls';

export const DonationUpdateItem = ({ item }) => {
    return (
        <Card style={styles.root}>
            <View style={styles.header}>
                <Image style={styles.userImage} source={{ 
                    uri: !item.share || item.User.photo == null ? urls.DEF_USER_IMAGE : 
                    item.User.photo }} />
                <View style={styles.user}>
                    <Text style={styles.textAmount}>{utils.currencyFormat(item.amount)}</Text>
                    <Text>{item.share ? item.User.name : "Anonymous"}</Text>
                </View>
                <Text style={styles.textTime}>{utils.isoDateToMoment(item.createdAt)}</Text>
            </View>
            <Text style={styles.textMessage}>{item.comment}</Text>
        </Card>
    )
}

const styles = StyleSheet.create({
    root: {
        margin: 4,
        padding: 8,
    },
    header: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-evenly",
        alignItems: "flex-start",
    },
    userImage: {
        width: 40,
        height: 40,
        borderRadius: 6,
    },
    user: {
        marginHorizontal: 16,
        flexGrow: 2
    },
    textAmount: {
        fontFamily: "Nunito-Regular",
        fontWeight: "700",
        color: res.PRIMARY_COLOR
    },
    textTime: {
        fontFamily: "Nunito-regular",
        color: 'gray',
        fontSize: 12
    },
    textMessage: {
        fontFamily: "Nunito-Regular",
        paddingVertical: 16
    }
})