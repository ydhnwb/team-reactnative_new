import React from 'react';
import { StyleSheet, View, Text, Image, FlatList } from 'react-native';
import * as res from '../values/colors';
import { FundraiserUpdateItem } from './FundraiserUpdateItem';
import { Button } from 'react-native-paper';



export const FundraiseUpdate = ({ logs = [], onTap = () => { } }) => {

    return (
        <View style={styles.borderContainer}>
            <Text style={styles.titleText}>Updates</Text>
            <FlatList
                style={{ marginTop: 16 }}
                scrollEnabled={false}
                keyExtractor={(item) => item.id.toString()}
                data={logs}
                renderItem={({ item }) => {
                    return (
                        <FundraiserUpdateItem item={item} />
                    )
                }}
            />

            {
                logs.length === 0 ? <Text>No campaign logs for now</Text>
                    :
                    <Button mode="outlined" style={{ alignSelf: "center" }} onPress={onTap}>
                        SHOW OLDER
            </Button>
            }

        </View>
    )
}

const styles = StyleSheet.create({
    titleText: {
        fontFamily: "Nunito-Regular",
        fontWeight: "bold",
        alignSelf: "stretch",
        fontSize: 16,
        color: 'black'
    },
    borderContainer: {
        padding: 16,
        borderRadius: 4,
        justifyContent: 'center',
        borderColor: 'gray',
        borderWidth: 0.2

    },
    content: {
        flexDirection: 'row',
        justifyContent: 'center'
    },
})