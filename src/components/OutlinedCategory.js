import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import * as res from './../values/colors';

export const OutlinedCategory = ({ category }) => {
    return (
        <View style={styles.borderContainer}>
            <Text style={styles.border}>{category.name}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    borderContainer: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',

    },
    border: {
        color: res.COLOR_ROSE,
        fontFamily: "Nunito-Regular",
        fontSize: 12,
        fontWeight: "400",
        paddingHorizontal: 6,
        borderRadius: 4,
        borderWidth: 1,
        borderColor: res.COLOR_ROSE,
    },
})