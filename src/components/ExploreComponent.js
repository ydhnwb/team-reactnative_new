import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View, Dimensions, Image } from 'react-native'
import * as res from './../values/colors';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import * as CampaignService from './../service/CampaignService';

const dim = Dimensions.get('window')


export default function FilterAndSort({ navigation, onSelectedCategory, saveState = false }) {
    const [itemSelected, setItemSelected] = useState('')
    const [screenWidth, setScreenWidth] = useState(dim.width)
    const [screenHeight, setScreenHeight] = useState(dim.height)
    const [categories, setCategories] = useState([])

    const getOrientation = (width, height) => width < height ? 'Potrait' : 'Landscape'

    const updateLayout = (event) => {
        const { width, height } = event.nativeEvent.layout
        let orientation = getOrientation(width, height)
        setScreenWidth(width)
        setScreenHeight(height)
    }

    const fetchCategory = async () => {
        const res = await CampaignService.getAllCategory()
        if (res.status === 200) {
            setCategories(res.data)
            if (res.data.length !== 0) {
                setItemSelected(res.data[0].name)
            }
        }
    }

    const onSelect = (category) => {
        onSelectedCategory(category)
        if (itemSelected == category.name) {
            setItemSelected('')
        } else {
            if (saveState) {
                setItemSelected(category.name)
            }
        }
    }

    const items = (category) => {
        return (
            <TouchableOpacity key={category.id.toString()} onPress={() => onSelect(category)}>
                {
                    saveState ?
                        <View style={itemSelected === category.name ? styles.itemSelected(screenWidth) : styles.item(screenWidth)}>
                            <Image source={{ uri: category.image }} style={{ width: 32, height: 32 }} />
                            <Text numberOfLines={1} style={{ fontSize: 12 }} ellipsizeMode='tail'>{category.name}</Text>
                        </View>
                        :
                        <View style={styles.item(screenWidth)}>
                            <Image source={{ uri: category.image }} style={{ width: 32, height: 32 }} />
                            <Text numberOfLines={1} style={{ fontSize: 12 }} ellipsizeMode='tail'>{category.name}</Text>
                        </View>
                }

            </TouchableOpacity>
        )

    }

    useEffect(() => {
        fetchCategory()
    }, [])

    return (
        <View style={{ justifyContent: 'center', flex: 1 }}>
            <View style={{ alignItems: 'stretch', alignContent: 'center', alignSelf: 'center', justifyContent: 'center', flexDirection: 'row', flex: 1, flexWrap: "wrap" }}>
                {
                    categories.map((it) => {
                        return items(it)
                    })
                }
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    item: function (w) {
        return {
            height: w * 0.20,
            width: w * 0.22,
            alignItems: 'center',
            borderRadius: 4,
            borderWidth: 0.5,
            borderColor: 'gray',
            padding: 8,
            margin: 2,
        }
    },
    itemSelected: function (w) {
        return {
            height: w * 0.20,
            width: w * 0.22,
            backgroundColor: res.COLOR_DUST,
            alignItems: 'center',
            borderRadius: 4,
            borderWidth: 0.5,
            borderColor: 'gray',
            padding: 8,
            margin: 2,
        }

    }
})
