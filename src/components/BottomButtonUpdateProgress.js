import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import * as res from './../values/colors';
import { Button } from 'react-native-paper';


export const BottomButtonUpdateProgress = ({ title, onTap = () => { console.log("hello") }, onShare = () => { } }) => {
    return (
        <View style={styles.body}>
            <View style={styles.share}>
                <Button icon="upload" mode="text" style={{ alignSelf: "stretch" }} onPress={onShare}>
                    SHARE
                </Button>
            </View>
            <View style={styles.updateProgress}>
                <Button color='white' mode="text" style={{ alignSelf: "stretch" }} onPress={onTap}>
                    {title}
                </Button>
            </View>

        </View>

    )
}

const styles = StyleSheet.create({
    body: {
        alignItems: 'center',
        alignContent: 'center',
        flex: 1,
        alignSelf: 'center',
        flexDirection: 'row',
    },
    titleText: {
        fontSize: 16,
        color: 'white'
    },
    titleTextDark: {
        fontSize: 16,
        color: 'black'
    },
    updateProgress: {
        alignItems: 'center',
        flex: 1,
        padding: 4,
        backgroundColor: res.COLOR_ROSE
    },
    share: {
        padding: 4,
        alignItems: 'center',
        flex: 1,
        backgroundColor: 'white'
    }
})