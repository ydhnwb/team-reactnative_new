import React from 'react';
import { Button } from 'react-native-paper';
import * as res from './../values/colors';

export const PrimaryButton = ({ title, onPress, isDisabled }) => {
    return (
        <Button disabled={isDisabled} color={res.PRIMARY_BUTTON_COLOR} mode="contained" onPress={onPress}>
            {title}
        </Button>
    )
}