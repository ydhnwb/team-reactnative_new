import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import * as res from './../values/colors'

export const LabelText = ({ title, isRequired = true }) => {
    return (
        <View style={{ flexDirection: 'row' }}>
            <Text style={styles.label}>{title}</Text>
            {
                isRequired ? <Text style={styles.labelRequired}>*</Text> : null
            }
        </View>
    )
}

const styles = StyleSheet.create({
    label: {
        fontFamily: "Nunito-Regular",
        fontWeight: "700"
    },
    labelRequired: {
        fontFamily: "Nunito-Regular",
        color: res.COLOR_ROSE
    }
})