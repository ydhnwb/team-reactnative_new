import { combineReducers } from 'redux'
import { userReducer } from './UserReducer'
import { campaignReducer } from './CampaignReducer'

export const PackedReducer = combineReducers({
    userReducer,
    campaignReducer
})