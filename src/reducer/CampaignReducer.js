import * as actions from './../utils/actions';

const initialState = {
    isShouldResetState: true,
    categories: []
}

export const campaignReducer = (state = initialState, action) => {
    switch(action.type){
        case actions.FETCH_CATEGORY:
            return {
                ...state,
                categories: action.payload.categories
            }
        case actions.RESET_CREATE_CAMPAIGN:
            return {
                ...state,
                isShouldResetState: action.payload.isShouldResetState
            }
        default:
            return state
    }
}