import * as actions from './../utils/actions';

const initialState = {
    token: null,
    user: null
}

export const userReducer = (state = initialState, action) => {
    switch(action.type){
        case actions.FETCH_TOKEN:
            if(action.payload.user){
                return {
                    ...state,
                    token: action.payload.token,
                    user: action.payload.user
                }
            }else{
                return {
                    ...state,
                    token: action.payload.token
                }
            }
        case actions.SIGN_OUT:
            return {
                ...state,
                token: null,
                user: null
            }
        default:
            return state
    }
}