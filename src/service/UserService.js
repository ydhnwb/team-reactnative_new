import * as urls from './urls';
import axios from 'axios';

export const login = async (email, password) => {
    try{
        let status;
        const payload = { email, password }
        const response = await axios.post(urls.URL_LOGIN, payload, { "Content-Type":"application/json"})
        .then((data) => {
            status = data.status;
            return data.data;
        }).catch((err) => {
            if(err.response){
                status = err.response.status
                return err.response.data
            }
            status = null
            return null
        })
        return { data: response, status: status }
    }catch(e){
        console.log(`Exception occured on login(): ${e}`);
        return { data: null, status: null }
    }
}

export const register = async (name, email, password) => {
    try{
        let status;
        const payload = { name, email, password }
        const response = await axios.post(urls.URL_REGISTER, payload, { "Content-Type":"application/json"})
        .then((data) => {
            status = data.status;
            return data.data;
        }).catch((err) => {
            if(err.response){
                status = err.response.status
                return err.response.data
            }
            status = null
            return null
        })
        return { data: response, status: status }
    }catch(e){
        console.log(`Exception occured on register(): ${e}`);
        return { data: null, status: null }
    }
}

export const profile = async (token) => {
    try{
        let status;
        const response = await axios.get(urls.URL_PROFILE, { 
            headers: {
                "Content-Type": "application/json",
                token: token
            }

         })
        .then((data) => {
            status = data.status;
            return data.data;
        }).catch((err) => {
            if(err.response){
                status = err.response.status
                return err.response.data
            }
            status = null
            return null
        })
        return { data: response, status: status }
    }catch(e){
        console.log(`Exception occured on profile(): ${e}`);
        return { data: null, status: null }
    }
}

export const profileUpdate = async (token, payload) => {
    try{
        console.log(payload)
        let status;
        const response = await axios.put(urls.URL_UPDATE_PROFILE, payload, { 
            headers: {
                "Content-Type": "application/json",
                token: token
            }
         })
        .then((data) => {
            status = data.status;
            return data.data;
        }).catch((err) => {
            if(err.response){
                status = err.response.status
                return err.response.data
            }
            status = null
            return null
        })
        return { data: response, status: status }
    }catch(e){
        console.log(`Exception occured on update profile(): ${e}`);
        return { data: null, status: null }
    }
}


export const profilePictureUpdate = async (token, image) => {
    try{
        console.log(image)
        let status;
        let data = new FormData();
        if (Object.keys(image).length !== 0) {
            data.append("photo", {
                name: image.fileName,
                type: image.type,
                uri: Platform.OS === "android" ? image.uri : image.uri.replace("file://", "")
            });
        }
        const response = await axios.put(urls.URL_UPDATE_PROFILE_PIC, data, { 
            headers: {
                Accept: "application/json",
                "Content-Type": "multipart/form-data",
                token: token
            }
         })
        .then((data) => {
            status = data.status;
            return data.data;
        }).catch((err) => {
            if(err.response){
                status = err.response.status
                return err.response.data
            }
            status = null
            return null
        })
        return { data: response, status: status }
    }catch(e){
        console.log(`Exception occured on update profile pic: ${e}`);
        return { data: null, status: null }
    }
}