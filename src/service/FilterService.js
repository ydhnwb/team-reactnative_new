import axios from 'axios';
import * as urls from './urls';
import * as utils from './../utils/utils';

export const searchAdvance = async (query, sort, categoryId, page = 1) => {
    try{
        let status;
        const response = await axios.get(urls.SEARCH_ADVANCE(query, sort, categoryId, page), { 
            headers: {
                "Content-Type": "application/json",
            }
         })
        .then((data) => {
            status = data.status;
            return data.data;
        }).catch((err) => {
            if(err.response){
                status = err.response.status
                return err.response.data
            }
            status = null
            return null
        })
        return { data: response, status: status }
    }catch(e){
        console.log(`Exception occured on get advance search: ${e}`);
        return { data: null, status: null }
    }
}