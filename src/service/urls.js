export const BASE_URL = "https://warm-tundra-23736.herokuapp.com/"
export const URL_LOGIN =`${BASE_URL}login`
export const URL_REGISTER = `${BASE_URL}`
export const URL_PROFILE = `${BASE_URL}formuser`
export const URL_UPDATE_PROFILE = `${BASE_URL}`
export const URL_UPDATE_PROFILE_PIC = `${BASE_URL}image`
export const URL_CATEGORY_ALL = `${BASE_URL}category`
export const URL_CAMPAIGN_CREATE = `${BASE_URL}campaign/add`
export const URL_CAMPAIGN_UPDATE_PICTURE = (campaignId) => `${BASE_URL}campaign/edit/image/${campaignId}`
export const URL_CAMPAIGN_UPDATE = (campaignId) => `${BASE_URL}campaign/edit/${campaignId}`
export const URL_CAMPAIGN_BY_ID = (id) => `${BASE_URL}campaign/${id}`
export const URL_CAMPAIGN_DELETE_BY_ID = (id) => `${BASE_URL}campaign/delete/${id}`
export const URL_CAMPAIGN_LOG = (campaignId, page = 1) => `${BASE_URL}campaignLog/${campaignId}/${page}`
export const URL_CAMPAIGN_DONATION = (campaignId) => `${BASE_URL}donate/campaign/${campaignId}`
export const URL_CAMPAIGN_LOG_CREATE = (campaignId) => `${BASE_URL}campaignLog/${campaignId}`
export const URL_CAMPAIGN_DONATION_CREATE = (campaignId) => `${BASE_URL}donate/campaign/${campaignId}`
export const URL_COMMENT = (campaignId) => `${BASE_URL}comment/${campaignId}`
export const URL_COMMENT_ADD = (campaignId) => `${BASE_URL}comment/add/${campaignId}`
export const URL_ALL_MY_CAMPAIGNS = `${BASE_URL}campaign/user`
export const URL_SEARCH_CAMPAIGN_WITH_CATEGORY = (query, categoryId) => `${BASE_URL}v2/campaign?search=${query}&CategoryId=${categoryId}`
export const URL_ALL_CAMPAIGNS_BY_CATEGORY = (categoryId, page = 1) => `${BASE_URL}discover/category/${categoryId}/${page}`
export const URL_ALL_MY_DONATIONS = `${BASE_URL}donate/campaign`


export const DEF_USER_IMAGE = "https://grandimageinc.com/wp-content/uploads/2015/09/icon-user-default.png"
export const CAMPAIGN_DISCOVER_FIRST = `${BASE_URL}discover/all/1`
export const CAMPAIGN_MOST_URGENT = `${BASE_URL}campaign/urgent`
export const CAMPAIGN_RAISED = `${BASE_URL}campaign/raised`
export const CAMPAIGN_SEARCH_BULK = (query, page = 1) => `${BASE_URL}discover/search?search=${query}&page=${page}`


export const SEARCH_ADVANCE = (query, filter, categoryId, page = 1) => `${BASE_URL}v2/filter?search=${query}&filter=${filter}&CategoryId=${categoryId}&page=${page}`