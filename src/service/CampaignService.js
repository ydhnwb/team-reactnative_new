import axios from 'axios';
import * as urls from './urls';
import * as utils from './../utils/utils';


export const discoverCampaign = async (page = 1) => {
    try {
        let status;
        const response = await axios.get(urls.CAMPAIGN_DISCOVER_FIRST, {
            headers: {
                "Content-Type": "application/json",
            }
        })
            .then((data) => {
                status = data.status;
                return data.data;
            }).catch((err) => {
                if (err.response) {
                    status = err.response.status
                    return err.response.data
                }
                status = null
                return null
            })
        return { data: response, status: status }
    } catch (e) {
        console.log(`Exception occured on get discover all campaign: ${e}`);
        return { data: null, status: null }
    }
}

export const mostUrgentCampaign = async () => {
    try {
        let status;
        const response = await axios.get(urls.CAMPAIGN_MOST_URGENT, {
            headers: {
                "Content-Type": "application/json",
            }
        })
            .then((data) => {
                status = data.status;
                return data.data;
            }).catch((err) => {
                if (err.response) {
                    status = err.response.status
                    return err.response.data
                }
                status = null
                return null
            })
        return { data: response, status: status }
    } catch (e) {
        console.log(`Exception occured on get most urgent campaign: ${e}`);
        return { data: null, status: null }
    }
}


export const raisedCampaign = async () => {
    try {
        let status;
        const response = await axios.get(urls.CAMPAIGN_RAISED, {
            headers: {
                "Content-Type": "application/json",
            }
        })
            .then((data) => {
                status = data.status;
                return data.data;
            }).catch((err) => {
                if (err.response) {
                    status = err.response.status
                    return err.response.data
                }
                status = null
                return null
            })
        return { data: response, status: status }
    } catch (e) {
        console.log(`Exception occured on get raised campaign: ${e}`);
        return { data: null, status: null }
    }
}


export const getAllCategory = async () => {
    try {
        let status;
        const response = await axios.get(urls.URL_CATEGORY_ALL, {
            headers: {
                "Content-Type": "application/json",
            }
        })
            .then((data) => {
                status = data.status;
                return data.data;
            }).catch((err) => {
                if (err.response) {
                    status = err.response.status
                    return err.response.data
                }
                status = null
                return null
            })
        return { data: response, status: status }
    } catch (e) {
        console.log(`Exception occured on get discover all campaign: ${e}`);
        return { data: null, status: null }
    }
}


export const createCampaign = async (token, campaign) => {
    try {
        let status;
        let data = new FormData();
        data.append("header_img", {
            name: campaign.header_img.fileName,
            type: campaign.header_img.type,
            uri: Platform.OS === "android" ? campaign.header_img.uri : campaign.header_img.uri.replace("file://", "")
        });
        Object.keys(campaign).forEach(key => {
            if (key !== "header_img") {
                if (key == 'goal') {
                    const parsed = parseInt(utils.clearDots(campaign[key].toString()))
                    console.log(parsed)
                    data.append(key, parsed)
                } else {
                    data.append(key, campaign[key]);
                }

            }
        });
        const response = await axios.post(urls.URL_CAMPAIGN_CREATE, data, {
            headers: {
                Accept: "application/json",
                "Content-Type": "multipart/form-data",
                token: token
            }
        })
            .then((data) => {
                status = data.status;
                return data.data;
            }).catch((err) => {
                if (err.response) {
                    status = err.response.status
                    return err.response.data
                }
                status = null
                return null
            })
        return { data: response, status: status }
    } catch (e) {
        console.log(`Exception occured on Create campaign(): ${e}`);
        return { data: null, status: null }
    }
}




export const getCampaignById = async (id, token = null) => {
    try {
        const header = { "Content-Type": "application/json" }
        if (token !== null) { header['token'] = token }
        let status;
        const response = await axios.get(urls.URL_CAMPAIGN_BY_ID(id), { header })
            .then((data) => {
                status = data.status;
                return data.data;
            }).catch((err) => {
                if (err.response) {
                    status = err.response.status
                    return err.response.data
                }
                status = null
                return null
            })
        return { data: response, status: status }
    } catch (e) {
        console.log(`Exception occured on get campaign by id: ${e}`);
        return { data: null, status: null }
    }
}


export const deleteCampaignById = async (id, token) => {
    try {
        let status;
        const response = await axios.delete(urls.URL_CAMPAIGN_DELETE_BY_ID(id), {
            headers: {
                "Content-Type": "application/json",
                token: token
            }
        })
            .then((data) => {
                status = data.status;
                return data.data;
            }).catch((err) => {
                if (err.response) {
                    status = err.response.status
                    return err.response.data
                }
                status = null
                return null
            })
        return { data: response, status: status }
    } catch (e) {
        console.log(`Exception occured on delete campaign: ${e}`);
        return { data: null, status: null }
    }
}


export const getCampaignLog = async (campaigniId, page = 1) => {
    try {
        const header = { "Content-Type": "application/json" }
        let status;
        const response = await axios.get(urls.URL_CAMPAIGN_LOG(campaigniId, page), { header })
            .then((data) => {
                status = data.status;
                return data.data;
            }).catch((err) => {
                if (err.response) {
                    status = err.response.status
                    return err.response.data
                }
                status = null
                return null
            })
        return { data: response, status: status }
    } catch (e) {
        console.log(`Exception occured on get campaign log: ${e}`);
        return { data: null, status: null }
    }
}

export const getCampaignDonation = async (campaignId) => {
    try {
        const header = { "Content-Type": "application/json" }
        console.log(urls.URL_CAMPAIGN_DONATION(campaignId))
        let status;
        const response = await axios.get(urls.URL_CAMPAIGN_DONATION(campaignId), { header })
            .then((data) => {
                status = data.status;
                return data.data;
            }).catch((err) => {
                if (err.response) {
                    status = err.response.status
                    return err.response.data
                }
                status = null
                return null
            })
        return { data: response, status: status }
    } catch (e) {
        console.log(`Exception occured on get campaign donation: ${e}`);
        return { data: null, status: null }
    }
}


export const updateCampaign = async (token, campaign) => {
    try {
        const data = { ...campaign }
        data['goal'] = parseInt(utils.clearDots(campaign['goal'].toString()))
        let status;
        const response = await axios.put(urls.URL_CAMPAIGN_UPDATE(campaign.id), data, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                token: token
            }
        })
            .then((data) => {
                status = data.status;
                return data.data;
            }).catch((err) => {
                if (err.response) {
                    status = err.response.status
                    return err.response.data
                }
                status = null
                return null
            })
        return { data: response, status: status }
    } catch (e) {
        console.log(`Exception occured on update campaign(): ${e}`);
        return { data: null, status: null }
    }
}

export const updateCampaignPicture = async (token, campaignId, image) => {
    try {
        let data = new FormData();
        data.append("header_img", {
            name: image.fileName,
            type: image.type,
            uri: Platform.OS === "android" ? image.uri : image.uri.replace("file://", "")
        });
        let status;
        const response = await axios.put(urls.URL_CAMPAIGN_UPDATE_PICTURE(campaignId), data, {
            headers: {
                Accept: "application/json",
                "Content-Type": "multipart/form-data",
                token: token
            }
        })
            .then((data) => {
                status = data.status;
                return data.data;
            }).catch((err) => {
                if (err.response) {
                    status = err.response.status
                    return err.response.data
                }
                status = null
                return null
            })
        return { data: response, status: status }
    } catch (e) {
        console.log(`Exception occured on update campaignPicture(): ${e}`);
        return { data: null, status: null }
    }
}


export const search = async (query, page = 1) => {
    try {
        const header = { "Content-Type": "application/json" }
        let status;
        const response = await axios.get(urls.CAMPAIGN_SEARCH_BULK(query, page), { header })
            .then((data) => {
                status = data.status;
                return data.data;
            }).catch((err) => {
                if (err.response) {
                    status = err.response.status
                    return err.response.data
                }
                status = null
                return null
            })
        return { data: response, status: status }
    } catch (e) {
        console.log(`Exception occured on get campaign donation: ${e}`);
        return { data: null, status: null }
    }
}


export const addCampaignLog = async (token, campaignId, log) => {
    try {
        if (log['StatusId'] === 2) {
            log['ammount'] = 0
        } else {
            log['ammount'] = parseInt(utils.clearDots(log['ammount']))
        }
        console.log(log)
        let status;
        const response = await axios.post(urls.URL_CAMPAIGN_LOG_CREATE(campaignId), log, {
            headers: {
                "Content-Type": "application/json",
                token: token
            }

        })
            .then((data) => {
                status = data.status;
                return data.data;
            }).catch((err) => {
                if (err.response) {
                    status = err.response.status
                    return err.response.data
                }
                status = null
                return null
            })
        return { data: response, status: status }
    } catch (e) {
        console.log(`Exception occured on add campaign donation: ${e}`);
        return { data: null, status: null }
    }
}


export const addCampaignDonation = async (token, campaignId, donation) => {
    try {
        donation['amount'] = parseInt(utils.clearDots(donation['amount']))
        console.log(donation)
        let status;
        const response = await axios.post(urls.URL_CAMPAIGN_DONATION_CREATE(campaignId), donation, {
            headers: {
                "Content-Type": "application/json",
                token: token
            }
        })
            .then((data) => {
                status = data.status;
                return data.data;
            }).catch((err) => {
                if (err.response) {
                    status = err.response.status
                    return err.response.data
                }
                status = null
                return null
            })
        return { data: response, status: status }
    } catch (e) {
        console.log(`Exception occured on add campaign donation: ${e}`);
        return { data: null, status: null }
    }
}

export const addComment = async (token, campaignId, payload) => {
    try {
        let body = { ...payload }
        body['date'] = (new Date()).toISOString().split('T')[0]
        let status;
        const response = await axios.post(urls.URL_COMMENT_ADD(campaignId), body, {
            headers: {
                "Content-Type": "application/json",
                token: token
            }
        })
            .then((data) => {
                status = data.status;
                return data.data;
            }).catch((err) => {
                if (err.response) {
                    status = err.response.status
                    return err.response.data
                }
                status = null
                return null
            })
        return { data: response, status: status }
    } catch (e) {
        console.log(`Exception occured on add comment: ${e}`);
        return { data: null, status: null }
    }
}


export const getComments = async (campaignId) => {
    try {
        let status;
        const response = await axios.get(urls.URL_COMMENT(campaignId), {
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then((data) => {
                status = data.status;
                return data.data;
            }).catch((err) => {
                if (err.response) {
                    status = err.response.status
                    return err.response.data
                }
                status = null
                return null
            })
        return { data: response, status: status }
    } catch (e) {
        console.log(`Exception occured on get comments: ${e}`);
        return { data: null, status: null }
    }
}


export const getAllMyCampaign = async (token) => {
    try {
        let status;
        const response = await axios.get(urls.URL_ALL_MY_CAMPAIGNS, {
            headers: {
                "Content-Type": "application/json",
                token: token
            }
        })
            .then((data) => {
                status = data.status;
                return data.data;
            }).catch((err) => {
                if (err.response) {
                    status = err.response.status
                    return err.response.data
                }
                status = null
                return null
            })
        return { data: response, status: status }
    } catch (e) {
        console.log(`Exception occured on get my campaigns: ${e}`);
        return { data: null, status: null }
    }
}


export const searchCampaignWithCategory = async (query, categoryId) => {
    try {
        let status;
        const response = await axios.get(urls.URL_SEARCH_CAMPAIGN_WITH_CATEGORY(query, categoryId), {
            headers: {
                "Content-Type": "application/json",
            }
        })
            .then((data) => {
                status = data.status;
                return data.data;
            }).catch((err) => {
                if (err.response) {
                    status = err.response.status
                    return err.response.data
                }
                status = null
                return null
            })
        return { data: response, status: status }
    } catch (e) {
        console.log(`Exception occured on search caampaign with category: ${e}`);
        return { data: null, status: null }
    }
}


export const allCampaignByCategory = async (categoryId, page = 1) => {
    try {
        let status;
        const response = await axios.get(urls.URL_ALL_CAMPAIGNS_BY_CATEGORY(categoryId, page), {
            headers: {
                "Content-Type": "application/json",
            }
        })
            .then((data) => {
                status = data.status;
                return data.data;
            }).catch((err) => {
                if (err.response) {
                    status = err.response.status
                    return err.response.data
                }
                status = null
                return null
            })
        return { data: response, status: status }
    } catch (e) {
        console.log(`Exception occured on all campaign by category: ${e}`);
        return { data: null, status: null }
    }
}



export const allMyDonations = async (token) => {
    try {
        let status;
        const response = await axios.get(urls.URL_ALL_MY_DONATIONS, {
            headers: {
                "Content-Type": "application/json",
                token: token
            }
        })
            .then((data) => {
                status = data.status;
                return data.data;
            }).catch((err) => {
                if (err.response) {
                    status = err.response.status
                    return err.response.data
                }
                status = null
                return null
            })
        return { data: response, status: status }
    } catch (e) {
        console.log(`Exception occured on all my donations: ${e}`);
        return { data: null, status: null }
    }
}