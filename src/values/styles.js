import * as res from './colors';
export const generalStyles = {
    input: {
        colors: {
            primary: res.PRIMARY_COLOR,
            background: 'transparent'
        },
    },
}