export const PRIMARY_COLOR = "#1D94A8"
export const PRIMARY_BUTTON_COLOR = '#A43F3C'
export const COLOR_ROSE = '#A43F3C'
export const COLOR_CHOPPER = "#A87B14"
export const COLOR_DUST = "#D7EBEE"
export const COLOR_PLATINUM = "#F4F4F4"
export const COLOR_SILK = "#F1EDE4"
export const COLOR_BG_LIGHT = "#f4f4f4"

export const COLOR_BABYDUST = "#E3ECEA"

export const ICON_SIZE = 28
export const ICON_SIZE_SMALL = 24
