import React, { useState } from 'react';
import { View, Text, StyleSheet, Dimensions, Alert } from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { generalStyles } from '../../values/styles';
import { TextInput, Button, Checkbox } from 'react-native-paper';
import AwesomeIcon from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import * as res from './../../values/colors';
import { CardForm } from '../../components/CardForm';
import { TransferForm } from '../../components/TransferForm';
import * as CampaignService from './../../service/CampaignService';
import * as utils from './../../utils/utils';
import { useSelector } from 'react-redux';

const dim = Dimensions.get('window')

export function DonationPage({ navigation, route }) {
    const userState = useSelector(state => state.userReducer)
    const [donation, setDonation] = useState({
        amount: '',
        share: false,
        comment: ''
    })

    const [methodSelected, setMethodSelected] = useState('card')
    const onChangeAmount = (amount) => {
        const newAmount = utils.formatRupiah(amount)
        setDonation({ ...donation, amount: newAmount })
    }

    const onChangeComment = (text) => setDonation({ ...donation, comment: text })
    const validate = () => {
        if (parseInt(utils.clearDots(donation.amount)) <= 0) {
            showDialog("Info", "Please enter a valid amount")
            return false
        }
        if (donation.comment.toString().trim() == '') {
            showDialog("Info", "Please enter your comment")
            return false
        }
        return true
    }

    const showDialog = (title, message) => Alert.alert(title, message,
        [
            { text: "OK", onPress: () => console.log("OK Pressed") }
        ],
        { cancelable: true }
    );

    const onSubmit = async () => {
        if (validate()) {
            const res = await CampaignService.addCampaignDonation(userState.token, route.params.campaign_id, donation)
            console.log(res)
            if (res.status === 200 && res.data.Success) {
                navigation.goBack()
            } else {
                showDialog("Failed", "Cannot process your donation")
            }
        }
    }

    return (
        <View style={styles.root}>
            <ScrollView style={styles.scroll}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Checkbox status={!donation.share ? 'checked' : 'unchecked'} onPress={() => setDonation({ ...donation, share: !donation.share })} />
                    <Text style={{ fontFamily: "Nunito-Regular" }}>Set as anonymous</Text>
                </View>
                <TextInput value={donation.amount} contextMenuHidden={true} keyboardType="number-pad" onChangeText={(text) => onChangeAmount(text)} theme={generalStyles.input} placeholder="Amount" />
                <TextInput onChangeText={(t) => onChangeComment(t)} theme={generalStyles.input} placeholder="Give them support" multiline={true} numberOfLines={6} />
                <View style={{ flexDirection: 'row', marginTop: 16, justifyContent: 'space-around' }}>
                    <TouchableOpacity onPress={() => setMethodSelected('card')}>
                        <View style={methodSelected === 'card' ? styles.borderSelected : styles.border}>
                            <FontAwesome5 name="credit-card" size={50} />
                            <Text style={{ fontFamily: "Nunito-Regular" }} numberOfLines={1} >Credit/debit card</Text>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => setMethodSelected('bank')}>
                        <View style={methodSelected === 'bank' ? styles.borderSelected : styles.border}>
                            <AwesomeIcon name="bank" size={50} />
                            <Text style={{ fontFamily: "Nunito-Regular" }} numberOfLines={1}>Bank transfer</Text>
                        </View>
                    </TouchableOpacity>

                </View>

                {
                    methodSelected === 'bank' ?
                        <View style={{ marginBottom: 56 }}>
                            <TransferForm user={route.params.user} />
                        </View>
                        : <CardForm />
                }

            </ScrollView>
            <View style={styles.bottomButton}>
                <Button color='white' mode="text" style={{ alignSelf: "stretch" }} onPress={onSubmit}>
                    DONATE
                </Button>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    root: {
        backgroundColor: 'white',
        flex: 1
    },
    scroll: {
        paddingHorizontal: 16,
        paddingTop: 16,
        marginBottom: 56
    },
    radio: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    radioText: {
        marginStart: 8,
        fontWeight: "700",
        fontSize: 18
    },
    bottomButton: {
        backgroundColor: res.COLOR_ROSE,
        padding: 4,
        position: 'absolute',
        justifyContent: "center",
        bottom: 0,
        left: 0,
        right: 0,
        alignItems: 'center',
    },
    border: {
        width: dim.width * 0.3,
        alignItems: 'center',
        padding: 16,
        borderRadius: 4,
        justifyContent: 'center',
        borderColor: 'gray',
        borderWidth: 0.2
    },
    borderSelected: {
        width: dim.width * 0.3,
        backgroundColor: res.COLOR_DUST,
        alignItems: 'center',
        padding: 16,
        borderRadius: 4,
        justifyContent: 'center',
        borderColor: 'gray',
        borderWidth: 0.2
    },
})