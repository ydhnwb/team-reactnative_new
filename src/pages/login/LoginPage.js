import React, { useState } from 'react';
import { View, Text, StyleSheet, Alert } from 'react-native';
import { IconButton } from 'react-native-paper';
import * as res from '../../values/colors';
import TaliKasih from './../../assets/talikasih.svg';
import { TextInput, HelperText, } from 'react-native-paper';
import { generalStyles } from '../../values/styles';
import { PrimaryButton } from '../../components/PrimaryButton';
import { SignGoogle } from '../../components/SignInGoogle';
import * as UserService from './../../service/UserService';
import * as validators from './../../utils/validators';
import * as storage from './../../utils/SharedPref';
import * as actions from './../../utils/actions';
import { useDispatch } from 'react-redux';

export function LoginPage({ navigation }) {
    const dispatch = useDispatch()
    const [isLoading, setIsLoading] = useState(false)
    const [loginForm, setLoginForm] = useState({
        email: '',
        password: ''
    })
    const [errors, setErrors] = useState({
        email: '',
        password: ''
    })

    const handleEmailText = (text) => setLoginForm({ ...loginForm, email: text })
    const handlePasswordText = (text) => setLoginForm({ ...loginForm, password: text })

    const goToRegister = () => navigation.navigate("Register")

    const fetchProfile = async (token) => {
        const res = await UserService.profile(token)
        if(res.status === 200 && (!res.data.hasOwnProperty("success") || res.data.success)){
            await storage.saveUserData(res.data.user)
            await storage.setToken(token)
            dispatch({ type: actions.FETCH_TOKEN, payload: { 
                token: token,
                user: res.data.user
            }})
            navigation.goBack()
        }else{
            showDialog("Failed", "Cannot get user data")
        }
    } 

    const onSubmit = async () => {
        if (validate()) {
            setIsLoading(true)
            const res = await UserService.login(loginForm.email, loginForm.password)
            setIsLoading(false)
            if(res.status === 200 && (!res.data.hasOwnProperty("success") || res.data.success)){
                fetchProfile(res.data.token)
            }else{
                const message = res.data !== null ? res.data.message : "Cannot login. Please check your credentials"
                showDialog("Failed", message)
            }
        }
    }

    const validate = () => {
        if (!validators.isValidEmail(loginForm.email.trim())) {
            setErrors({ ...errors, email: 'Email is not valid', password: "" })
            return false
        }
        if (!validators.isValidPassword(loginForm.password.trim())) {
            setErrors({ ...errors, email: '', password: 'Password must contains at least 6 chars with letter and number' })
            return false
        }
        setErrors({ ...errors, email: "", password: "" })
        return true
    }

    const showDialog = (title, message) =>
        Alert.alert(title, message,
            [
                { text: "OK", onPress: () => console.log("OK Pressed") }
            ],
            { cancelable: true }
        );


    return (
        <View style={styles.root}>
            <View style={styles.backButton}>
                <IconButton
                    icon="arrow-left"
                    color={res.PRIMARY_COLOR}
                    size={res.ICON_SIZE}
                    onPress={() => navigation.goBack()} />
            </View>

            <View style={styles.container}>
                <View style={styles.icon}>
                    <TaliKasih style={styles.icon} />
                    <Text style={styles.title}>
                        <Text style={styles.textBold}>Tali</Text>
                        <Text>Kasih</Text>
                    </Text>
                </View>

                <TextInput onChangeText={handleEmailText} theme={generalStyles.input} placeholder="Email" />
                <HelperText type="error" visible={errors.email !== ''}>
                    {errors.email}
                </HelperText>
                <TextInput onChangeText={handlePasswordText} theme={generalStyles.input} secureTextEntry={true} placeholder="Password" />
                <HelperText type="error" visible={errors.password !== ''}>
                    {errors.password}
                </HelperText>
                <Text style={styles.forgotPassword}>forgot password?</Text>
                <PrimaryButton isDisabled={isLoading} onPress={onSubmit} title="Login" />
                <Text style={[{ alignSelf: "center" }, styles.textNewUser]}>
                    <Text>New user?</Text>
                    <Text onPress={goToRegister} style={styles.textPrimaryColor}> Create an account </Text>
                </Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    root: {
        flex: 1,
    },
    backButton: {
        position: 'absolute',
        justifyContent: "center"
    },
    continueGoogle: {
        padding: 16,
        position: 'absolute',
        alignSelf: 'center',
        justifyContent: "center",
        bottom: 0
    },
    icon: {
        alignSelf: "center"
    },
    forgotPassword: {
        alignSelf: "flex-end",
        marginVertical: 8,
        textDecorationLine: 'underline'
    },
    container: {
        marginHorizontal: 16,
        flex: 1,
        alignSelf: 'stretch',
        alignItems: 'stretch',
        justifyContent: 'center'
    },
    content: {
        marginTop: 80,
        marginStart: 16,
        marginEnd: 16
    },
    title: {
        marginTop: 8,
        fontSize: 22,
        color: res.PRIMARY_COLOR
    },
    textNewUser: {
        marginTop: 16,
        textDecorationLine: 'underline'
    },
    textPrimaryColor: {
        color: res.PRIMARY_COLOR
    },
    textBold: {
        fontWeight: "bold"
    },
    form: {
        marginHorizontal: 16,
        flex: 1,
        alignSelf: 'stretch',
        alignItems: 'center',
        justifyContent: 'center'
    }
})