import React, { useEffect, useState } from 'react';
import { FlatList, View } from 'react-native';
import * as CampaignService from './../../service/CampaignService';
import { CommentItem } from './../../components/CommentItem';

export function CommentPage({ route }) {
    const [currentComments, setCurrentComments] = useState()

    const fetchComments = async () => {
        const res = await CampaignService.getComments(route.params)
        if (res.status === 200 && res.data.success) {
            setCurrentComments(res.data.comments)
        }
    }

    useEffect(() => {
        fetchComments()
    }, [])

    return (
        <View style={{ flex: 1 }}>
            <FlatList
                keyExtractor={(item) => item.id.toString()}
                data={currentComments}
                renderItem={({ item }) => {
                    return (
                        <CommentItem comment={item} />
                    )
                }}
            />
        </View>

    )
}