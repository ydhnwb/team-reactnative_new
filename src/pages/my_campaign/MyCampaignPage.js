import React, { useEffect, useState } from 'react';
import { Text, View, StyleSheet, ActivityIndicator } from 'react-native';
import { CampaignItem } from '../../components/CampaignItem';
import { FlatList } from 'react-native-gesture-handler';
import * as res from './../../values/colors';
import { BottomButton } from '../../components/BottomButton';
import { TabActions } from '@react-navigation/native';
import * as CampaignService from './../../service/CampaignService';
import { useSelector } from 'react-redux';


export function MyCampaignPage({ navigation }) {
    const [isLoading, setIsLoading] = useState(false)
    const userState = useSelector(state => state.userReducer)
    const [myCampaigns, setMyCampaigns] = useState([])

    const goToDetailCampaign = (campaign) => navigation.navigate("Fundraiser", { campaign_id: campaign.id })

    const fetchMyCampaigns = async () => {
        setIsLoading(true)
        const res = await CampaignService.getAllMyCampaign(userState.token)
        setIsLoading(false)
        if (res.status === 200 && res.data.success) {
            setMyCampaigns(res.data.result)
        }
    }

    const onTap = () => {
        navigation.goBack()
        const purpose = TabActions.jumpTo('Create campaign')
        navigation.dispatch(purpose);
    }

    useEffect(() => {
        fetchMyCampaigns()
    }, [])


    return (
        <View style={{ flex: 1 }}>
            {
                isLoading ?
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <ActivityIndicator size="large" color={res.PRIMARY_COLOR}/>
                    </View>
                    :
                    <View>
                        <View style={styles.bottomButton}>
                            <BottomButton title="CREATE CAMPAIGN" onTap={onTap} />
                        </View>
                        <FlatList
                            style={{ marginBottom: 46 }}
                            keyExtractor={(item) => item.id.toString()}
                            data={myCampaigns}
                            renderItem={({ item }) => {
                                return <CampaignItem onTap={goToDetailCampaign} campaign={item} />
                            }}
                        />
                    </View>

            }


        </View>
    )
}

const styles = StyleSheet.create({
    bottomButton: {
        backgroundColor: res.COLOR_ROSE,
        padding: 4,
        position: 'absolute',
        justifyContent: "center",
        bottom: 0,
        left: 0,
        right: 0,
        alignItems: 'center',
    },
})