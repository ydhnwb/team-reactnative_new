import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, FlatList, ActivityIndicator } from 'react-native';
import * as res from './../../values/colors';
import Icon from 'react-native-vector-icons/Feather'
import * as CampaignService from './../../service/CampaignService';
import { CampaignItem } from '../../components/CampaignItem';
import * as FilterService from './../../service/FilterService';

export function SearchResultPage({ navigation, route }) {
    const [s, setS] = useState('')
    const [isLoading, setIsLoading] = useState(false)
    const [filterProperties, setFilterProperties] = useState({
        categoryId: 0,
        sortBy: ''
    })
    const [pages, setPages] = useState({
        on_page: 0,
        total_page: 0
    })
    const [campaigns, setCampaigns] = useState([])

    const goToCampaignDetail = (campaign) => navigation.navigate("Fundraiser", {
        campaign_id: campaign.id,
        CategoryId: campaign.CategoryId,
    })


    const handleAdvanceSearch = async (fromZero, categoryId, sortBy, page = 1) => {
        setIsLoading(true)
        const res = await FilterService.searchAdvance(route.params.query, sortBy, categoryId, page)
        setIsLoading(false)
        if (res.status === 200 && (res.data.hasOwnProperty("status") && res.data.status)) {
            setPages({ ...pages, on_page: parseInt(res.data.on_page), total_page: res.data.total_pages })
            if (fromZero) {
                setCampaigns(res.data.document)
            } else {
                setCampaigns(campaigns.concat(res.data.document))
            }
        } else {
            console.log("No data")
        }
    }


    const search = async () => {
        if (pages.on_page == 0) {
            setIsLoading(true)
            const res = await CampaignService.search(route.params.query, 1)
            setIsLoading(false)
            if (res.status === 200 && res.data.status) {
                setPages({ ...pages, on_page: parseInt(res.data.on_page), total_page: res.data.total_pages })
                setCampaigns(res.data.document)
            }
        } else {
            const p = pages.on_page + 1
            if (p <= pages.total_page) {
                const res = await CampaignService.search(route.params.query, p)
                if (res.status === 200 && res.data.status) {
                    setPages({ ...pages, on_page: parseInt(res.data.on_page), total_page: res.data.total_pages })
                    setCampaigns(campaigns.concat(res.data.document))
                }
            }
        }
    }

    const onEndReached = () => {
        if (filterProperties.sortBy == '' && filterProperties.categoryId == 0) {
            search()
        } else {
            const p = pages.on_page + 1
            if (p <= pages.total_page) {
                handleAdvanceSearch(false, filterProperties.categoryId, filterProperties.sortBy, p)
            }
        }
    }

    const goToFilter = () => {
        navigation.navigate("Filter and sort", {
            onFilterSelect: function (categoryId, sortBy) {
                setCampaigns([])
                setPages({ ...pages, on_page: 0, total_page: 0 })
                setFilterProperties({ ...filterProperties, categoryId: categoryId, sortBy: sortBy })
                handleAdvanceSearch(true, categoryId, sortBy, 1)
            }
        })
    }

    useEffect(() => {
        search()
    }, [])

    return (
        <View style={{ flex: 1 }}>
            {
                isLoading ?
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <ActivityIndicator size="large" color={res.PRIMARY_COLOR} />
                    </View>
                    :
                    <View style={{ flex: 1}}>
                        <Text style={{ fontFamily: "Nunito-Regular" }}>{s}</Text>
                        <FlatList
                            onEndReached={onEndReached}
                            onEndReachedThreshold={0.5}
                            marginHorizontal={8}
                            data={campaigns}
                            keyExtractor={(item) => item.id.toString()}
                            renderItem={({ item }) => {
                                return (
                                    <CampaignItem campaign={item} onTap={goToCampaignDetail} />
                                )
                            }}
                        />
                        <View style={styles.icon}>
                            <Icon onPress={goToFilter} name="filter" size={28} color={res.PRIMARY_COLOR} />
                        </View>
                    </View>

            }
        </View>
    )
}

const styles = StyleSheet.create({
    root: {
        marginBottom: 56,
    },

    icon: {
        padding: 8,
        backgroundColor: res.COLOR_BG_LIGHT,
        borderRadius: 8,
        position: "absolute",
        bottom: 0,
        right: 0,
        marginVertical: 16,
        marginHorizontal: 16,
        elevation: 10,
        position: "absolute",
    },

})
