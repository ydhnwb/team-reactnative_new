import React from 'react'
import { StyleSheet, Text, View, ScrollView, } from 'react-native'
import { TextInput, List, Divider } from 'react-native-paper'
import ExploreComponent from '../../components/ExploreComponent'
import { generalStyles } from './../../values/styles';
import { SearchButton } from '../../components/SearchButton';
import * as res from './../../values/colors';


export default function ExplorePage({ navigation }) {
    const [text, setText] = React.useState('');

    const goToSearchResultPage = () => {
        if (text !== '') {
            navigation.navigate("Search result", {
                query: text,
            })
        }

    }

    const onSelectCategory = (category) => navigation.navigate("By category", category)

    return (
        <ScrollView style={{ backgroundColor: 'white' }}>
            <View style={{ justifyContent: 'space-between', flexDirection: 'row', flex: 1 }}>
                <View style={{ margin: 16, flexDirection: 'row', flex: 1 }}>
                    <TextInput onChangeText={(t) => setText(t)} theme={generalStyles.input} height={32} placeholder="Search" style={{ flexGrow: 3, marginVertical: 4, marginHorizontal: 4, height: 44, justifyContent: 'center' }} />
                    <SearchButton onTap={goToSearchResultPage} />
                </View>
            </View>
            <Text style={{ marginHorizontal: 16, fontFamily: "Nunito-Regular", fontSize: 15, marginVertical: 10 }}>Find by Category</Text>
            <ExploreComponent onSelectedCategory={onSelectCategory} />
            
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    sortItem: {
    },
    sortItemSelected: {
        backgroundColor: res.COLOR_DUST
    }
})
