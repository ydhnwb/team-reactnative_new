import React from 'react';
import { View, Alert } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { List, Divider } from 'react-native-paper';
import { useDispatch, useSelector } from 'react-redux';
import * as actions from './.././../../utils/actions';
import * as storage from './../../../utils/SharedPref';

export function MoreTab({ navigation }) {
    const dispatch = useDispatch()
    const userState = useSelector(state => state.userReducer)

    const askSignout = () => showDialog("Sign out", "Do you want to sign out from this app?")

    const doLogout = async () => {
        await storage.removeToken()
        await storage.removeUser()
        dispatch({ type: actions.SIGN_OUT })
    }

    const showDialog = (title, message) =>
        Alert.alert(title, message,
            [{
                text: "Cancel",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel"
            },
            { text: "OK", onPress: () => doLogout() }
            ],
            { cancelable: true }
        );


    return (
        <ScrollView style={{ backgroundColor: 'white' }}>
            <List.Section>
                <View>
                    <List.Item onPress={()=> navigation.navigate("About")} title="About" left={() => <List.Icon icon="information-outline" />} />
                    <Divider />
                </View>
                <View>
                    <List.Item onPress={() => navigation.navigate("Contact")} title="Contact us" left={() => <List.Icon icon="phone" />} />
                    <Divider />
                </View>
                <View>
                    <List.Item onPress={() => navigation.navigate("FAQ")} title="Faq" left={() => <List.Icon icon="chat" />} />
                    <Divider />
                </View>
                {
                    userState.token !== null ?
                        <View>
                            <List.Item onPress={askSignout} title="Logout" left={() => <List.Icon color='red' icon="exit-to-app" />} />
                            <Divider />
                        </View>
                        : null
                }

            </List.Section>

        </ScrollView>
    )
}