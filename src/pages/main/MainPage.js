import React, { useEffect } from 'react';
import { View, Text } from 'react-native';
import { AppBar } from './../../components/AppBar';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { DonateTab } from './donate/DonateTab';
import { CampaignTab } from './create-campaign/CampaignTab';
import { MyAccountTab } from './my-account/MyAccountTab';
import { MoreTab } from './more/MoreTab';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import * as res from './../../values/colors';
import * as storage from './../../utils/SharedPref';
import * as actions from './../../utils/actions';
import { useDispatch } from 'react-redux';
import * as CampaignService from './../../service/CampaignService';


const Tab = createBottomTabNavigator();

export function MainPage() {
    const dispatch = useDispatch()

    const fetchCategories = async () => {
        console.log("fetching categories....")
        const res = await CampaignService.getAllCategory()
        if (res.status === 200) {
            const categoriesStructured = []
            res.data.map((category) => {
                category['label'] = category.name
                category['value'] = category.name.toLowerCase()
                categoriesStructured.push(category)
            })
            dispatch({
                type: actions.FETCH_CATEGORY, payload: {
                    categories: categoriesStructured
                }
            })
        }
    }

    const checkIsLoggedIn = async () => {
        const token = await storage.getToken()
        const user = await storage.getUser()
        dispatch({ type: actions.FETCH_TOKEN, payload: { token: token, user: user } })
    }


    useEffect(() => {
        checkIsLoggedIn()
        fetchCategories()
    }, [])

    return (
        <Tab.Navigator lazy={true} initialRouteName="Donate" screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
                let iconName;

                if (route.name === 'Donate') {
                    iconName = focused ? 'star' : 'star-outline';
                } else if (route.name === 'Create campaign') {
                    iconName = focused ? 'plus-box' : 'plus-box-outline';
                } else if (route.name === 'My account') {
                    iconName = focused ? 'account' : 'account-outline';
                } else {
                    iconName = focused ? 'dots-horizontal' : 'dots-horizontal';
                }
                return <MaterialCommunityIcons name={iconName} size={size} color={color} />
            },
        })}
            tabBarOptions={{
                activeTintColor: res.PRIMARY_COLOR,
                inactiveTintColor: 'gray',
            }}>
            <Tab.Screen name="Donate" component={DonateTab} />
            <Tab.Screen name="Create campaign" component={CampaignTab} />
            <Tab.Screen name="My account" component={MyAccountTab}/>
            <Tab.Screen name="More" component={MoreTab} />
        </Tab.Navigator>
    )
}