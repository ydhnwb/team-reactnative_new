import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, ScrollView, FlatList, ActivityIndicator } from 'react-native';
import * as res from './../../../values/colors'
import { CampaignItem } from '../../../components/CampaignItem';
import * as CampaignService from './../../../service/CampaignService';
import CarouselComponent from '../../../components/CarouselComponent';


export const DonateTab = ({ navigation }) => {
    const [isLoading, setIsLoading] = useState(false)
    const [discoverCampaign, setDiscoverCampaign] = useState({
        document: []
    })
    const [mostUrgentCampaigns, setMostUrgentCampaigns] = useState([])
    const [raisedCampaign, setRaisedCampaign] = useState([])

    const goToCampaignDetail = (campaign) => {
        navigation.navigate("Fundraiser", {
        campaign_id: campaign.id,
        CategoryId: campaign.CategoryId,
    })}
    const newests = () => setNewest(newests)

    const fetchDiscoverCampaign = async () => {
        const res = await CampaignService.discoverCampaign()
        if (res.status === 200) {
            setDiscoverCampaign(res.data)
        }
    }

    const fetchMostUrgentCampaign = async () => {
        const res = await CampaignService.mostUrgentCampaign()
        if (res.status === 200 && res.data.success) {
            setMostUrgentCampaigns(res.data.campaign)
        }
    }

    const fetchRaisedCampaign = async () => {
        const res = await CampaignService.raisedCampaign()
        if (res.status === 200 && res.data.Success) {
            setRaisedCampaign(res.data.Result)
        }
    }

    const getData = async () => {
        setIsLoading(true)
        //looks like i will call api request sequentially for easier load
        await fetchDiscoverCampaign()
        await fetchMostUrgentCampaign()
        await fetchRaisedCampaign()
        setIsLoading(false)
    }

    useEffect(() => {
        getData()
    }, [])

    return (
        <View style={styles.root}>
            {
                isLoading ?
                    <View style={styles.loadingView}>
                        <ActivityIndicator size="large" color={res.PRIMARY_COLOR} />
                    </View>
                    : 
                    <ScrollView>

                    <CarouselComponent />
    
                    <View style={styles.container}>
                        <Text style={styles.text}>Newest</Text>
                        <FlatList
                            marginHorizontal={8}
                            horizontal
                            showsHorizontalScrollIndicator={false}
                            keyExtractor={(item) => item.id.toString()}
                            data={discoverCampaign.document}
                            renderItem={({ item }) => (
                                <CampaignItem campaign={item} small={true} onTap={goToCampaignDetail} />
                            )}
                        />
    
                        <Text style={styles.text}>Most Urgent</Text>
                        <FlatList
                            marginHorizontal={8}
                            horizontal
                            showsHorizontalScrollIndicator={false}
                            keyExtractor={(item) => item.id.toString()}
                            data={mostUrgentCampaigns}
                            renderItem={({ item }) => (
                                <CampaignItem campaign={item} small={true} onTap={goToCampaignDetail} />
                            )}
                        />
    
                        <Text style={styles.text}>Gained Momentum</Text>
                        <FlatList
                            marginHorizontal={8}
                            horizontal
                            showsHorizontalScrollIndicator={false}
                            keyExtractor={(item) => item.id.toString()}
                            data={raisedCampaign}
                            renderItem={({ item }) => (
                                <CampaignItem campaign={item} small={true} onTap={goToCampaignDetail} />
                            )}
                        />
                    </View>
    
    
                </ScrollView>
    
            }

        </View >
    )
}

const styles = StyleSheet.create({
    root: {
        flex: 1,
    },

    loadingView : {  
        flex: 1, 
        alignItems: 'center', 
        justifyContent: 'center', 
        alignSelf: 'center', 
        alignContent: 'center'
    },
    container: {
        marginBottom: 16
    },

    linearGradient: {

    },

    text: {
        left: 20,
        fontSize: 20,
        fontFamily: "Nunito-SemiBold",
        marginBottom: 10,
        marginTop: 6,

    },

    wrapper: {
        top: 16,
        height: 250,
        shadowColor: '#000'
    },

    slide1: {
        marginLeft: 15,
        marginRight: 15,
        width: "100%",
        height: "88%",
        position: "absolute",
        justifyContent: 'center',
        alignItems: 'center',
    },

    slide2: {
        marginLeft: 15,
        marginRight: 15,
        width: "100%",
        height: "88%",
        position: "absolute",
        justifyContent: 'center',
        alignItems: 'center',
    },

    icon: {
        padding: 8,
        backgroundColor: res.COLOR_BG_LIGHT,
        borderRadius: 8,
        position: "absolute",
        bottom: 0,
        right: 0,
        marginVertical: 28,
        marginHorizontal: 10,
        elevation: 10,
        position: "absolute",
    },

})
