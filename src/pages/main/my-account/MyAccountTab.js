import React, { useState } from 'react';
import { Text, View, Dimensions, StyleSheet, Image } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { List, Divider } from 'react-native-paper';
import { NotLoggedInView } from '../../../components/NotLoggedInView';
import * as res from './../../../values/colors';
import { useSelector } from 'react-redux';
import * as urls from './../../../service/urls';

export function MyAccountTab({ navigation }) {
    const userState = useSelector(state => state.userReducer)

    const goToMyDonationPage = () => navigation.navigate("My donations")
    const goToMyCampaignPage = () => navigation.navigate("My campaigns")
    const goToLoginPage = () => navigation.navigate("Login")
    const goToRegisterPage = () => navigation.navigate("Register")
    const goToEditProfile = () => navigation.navigate("Edit profile")

    const showBankInfo = () => {
        if(userState.user?.bank_name !== null && userState.user?.bank_account !== null){
            return `${userState.user.bank_name} - ${userState.user.bank_account}`
        }
        return "No bank account provided"
    }

    return (
        <View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'white' }}>
            {
                userState.token === null ?
                    <View style={styles.notLoggedInView}>
                        <NotLoggedInView onLoginTap={goToLoginPage} onRegisterTap={goToRegisterPage} />
                    </View>

                    :
                    <ScrollView>

                        <View style={styles.root}>
                            <Image style={styles.img} source={{
                                uri: userState.user.photo !== null ? userState.user.photo : urls.DEF_USER_IMAGE
                            }} />
                            <Text onPress={goToEditProfile} style={styles.editProfileText}>Edit profile</Text>

                            <Text style={styles.profileHelperText}>Name</Text>
                            <Text style={styles.profileText}>{userState.user.name}</Text>

                            <Text style={styles.profileHelperText}>Email</Text>
                            <Text style={styles.profileText}>{userState.user.email}</Text>

                            <Text style={styles.profileHelperText}>Bank Info</Text>
                            <Text style={styles.profileText}>{showBankInfo()}</Text>


                        </View>

                        <List.Section >
                            <View>
                                <List.Item onPress={goToMyDonationPage} title="My donations" right={() => <List.Icon icon="arrow-right" />} />
                                <Divider />
                            </View>
                            <View>
                                <List.Item onPress={goToMyCampaignPage} title="My campaigns" right={() => <List.Icon color="#000" icon="arrow-right" />} />
                                <Divider />
                            </View>
                        </List.Section>

                    </ScrollView>
            }
        </View>


    )
}

const styles = StyleSheet.create({
    root: {
        marginHorizontal: 16,
        marginBottom: 16
    },
    img: {
        alignSelf: 'center',
        backgroundColor: 'gray',
        flex: 1,
        width: 150,
        height: 150,
        margin: 16,
    },
    editProfileText: {
        alignSelf: 'center',
        color: res.PRIMARY_COLOR,
        marginBottom: 16,
        textDecorationLine: 'underline'
    },
    profileHelperText: {
        color: 'gray'
    },
    profileText: {
        fontSize: 18,
        fontWeight: "600",
        marginBottom: 16
    },
    notLoggedInView: {
        alignItems: 'center',
        justifyContent: "center",
        alignContent: 'center'

    },
    notLoggedInButtons: {

    }

})