import React, { useEffect, useState } from 'react';
import { FlatList, View, Text, ActivityIndicator } from 'react-native';
import { CampaignItem } from '../../components/CampaignItem';
import { OutlinedCategory } from '../../components/OutlinedCategory';
import * as CampaignService from './../../service/CampaignService';
import * as res from './../../values/colors';


export function ByCategoryPage({ navigation, route }) {
    const [pages, setPages] = useState({
        on_page: 0,
        total_page: 0
    })
    const [campaigns, setCampaigns] = useState([])
    const [isLoading, setIsLoading] = useState(false)

    const goToCampaignDetail = (campaign) => navigation.navigate("Fundraiser", { 
        campaign_id: campaign.id,
        CategoryId: campaign.CategoryId
     })

    const fetchAllCampaignByCategory = async (page = 1) => {
        const res = await CampaignService.allCampaignByCategory(route.params.id, page)
        console.log(res)
        if (res.status === 200) {
            setPages({ ...pages, on_page: parseInt(res.data.on_page), total_page: res.data.total_pages })
            if (page == 1) {
                setCampaigns(res.data.document)
            } else {
                setCampaigns(campaigns.concat(res.data.document))
            }

        } else {
            console.log("Cannot fetch data")
        }
    }

    const handleFetch = () => {
        if (pages.on_page == 0) {
            setIsLoading(true)
            fetchAllCampaignByCategory(1)
            setIsLoading(false)
        } else {
            const p = pages.on_page + 1
            fetchAllCampaignByCategory(p)
        }
    }

    useEffect(() => {
        handleFetch()
    }, [])

    return (
        <View style={{ flex: 1 }}>
            {
                isLoading ?
                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <ActivityIndicator size="large" color={res.PRIMARY_COLOR}/>
                </View>
                :

                    <FlatList
                        ListHeaderComponent={() => {
                            return (
                                <View style={{ margin: 16 }}>
                                    <OutlinedCategory category={route.params} />
                                </View>
                            )
                        }}
                        keyExtractor={(item) => item.id.toString()}
                        data={campaigns}
                        renderItem={({ item }) => {
                            return (
                                <CampaignItem campaign={item} onTap={goToCampaignDetail} />
                            )
                        }}
                    />
            }
        </View>
    )
}