import React, { useEffect, useState } from 'react';
import { View, ActivityIndicator, FlatList } from 'react-native';
import * as CampaignService from './../../service/CampaignService';
import { DonationUpdateItem } from './../../components/DonationUpdateItem';

export function CampaignDonationPage({ navigation, route }) {
    const [currentDonations, setCurrentDonations] = useState([])


    const fetchDonations = async () => {
        console.log("fetching...")
        const res = await CampaignService.getCampaignDonation(route.params)
        console.log(res)
        if (res.status === 200) {
            setCurrentDonations(res.data)
        }else{
            console.log("exception")
        }
    }

    useEffect(() => {
        console.log(route.params)
        fetchDonations()
    }, [])

    return (
        <View style={{ flex: 1 }}>
            <FlatList
                style={{ marginHorizontal: 8 }}
                keyExtractor={(item) => item.id.toString()}
                data={currentDonations}
                renderItem={({ item }) => {
                    return (
                        <DonationUpdateItem item={item} />
                    )
                }}
            />
        </View>
    )
}