import React, { useState } from 'react';
import { View, Text, StyleSheet, Alert } from 'react-native';
import { IconButton, HelperText } from 'react-native-paper';
import * as res from '../../values/colors';
import TaliKasih from './../../assets/talikasih.svg';
import { TextInput } from 'react-native-paper';
import { generalStyles } from '../../values/styles';
import { PrimaryButton } from '../../components/PrimaryButton';
import { SignGoogle } from '../../components/SignInGoogle';
import * as validators from './../../utils/validators';
import * as UserService from './../../service/UserService';
import * as storage from './../../utils/SharedPref';
import * as actions from './../../utils/actions';
import { ScrollView } from 'react-native-gesture-handler';
import { useDispatch } from 'react-redux';

export function RegisterPage({ navigation }){
    const dispatch = useDispatch()
    const [isLoading, setIsLoading] = useState(false)
    const [errors, setErrors] = useState({
        name:'',
        email:'',
        password: '',
        confirm_password: ''
    })
    const [registerForm, setRegisterForm] = useState({
        name:'',
        email:'',
        password: '',
        confirm_password: ''
    })

    const fetchProfile = async (token) => {
        console.log(token)
        const res = await UserService.profile(token)
        if(res.status === 200 && (!res.data.hasOwnProperty("success") || res.data.success)){
            await storage.saveUserData(res.data.user)
            await storage.setToken(token)
            dispatch({ type: actions.FETCH_TOKEN, payload: { 
                token: token,
                user: res.data.user
            }})
            navigation.popToTop()
        }else{
            showDialog("Failed", "Cannot get user data")
        }
    } 

    const onSubmit = async () => {
        if(validate()){
            setIsLoading(true)
            const res = await UserService.register(registerForm.name, registerForm.email, registerForm.password)
            setIsLoading(false)
            if(res.status === 200 && (!res.data.hasOwnProperty("success") || res.data.success)){
                fetchProfile(res.data.token)
            }else{
                const message = res.data !== null ? res.data.message : "Cannot Register. Please check your data"
                showDialog("Failed", message)
            }
        }
    }

    const showDialog = (title, message) =>
    Alert.alert(title, message,
        [
            { text: "OK", onPress: () => console.log("OK Pressed") }
        ],
        { cancelable: true }
    );

    const validate = () => {
        if(registerForm.name.trim() === ''){
            setErrors({...errors, name: "Name cannot be empty", email: '', password: '', confirm_password: ''})
            return false
        }
        if(!validators.isValidEmail(registerForm.email.trim())){
            setErrors({...errors, name: '', email: 'Email not valid', password: '', confirm_password: ''})
            return false
        }
        if(!validators.isValidPassword(registerForm.password.trim())){
            setErrors({...errors, name: '', email: '', password: 'Password must contains at least 6 chars with letter and number', confirm_password: ''})
            return false
        }
        if(registerForm.password !== registerForm.confirm_password){
            setErrors({...errors, name: '', email: '', password: '', confirm_password: 'Confirm password not match'})
            return false
        }
        setErrors({...errors, name: '', email: '', password: '', confirm_password: ''})
        return true
    }


    return(
        <ScrollView style={styles.root}>
            <View style={styles.backButton}>
                <IconButton
                    icon="arrow-left"
                    color={res.PRIMARY_COLOR}
                    size={res.ICON_SIZE}
                    onPress={() => navigation.goBack()} />
            </View>

            <View style={styles.container}>
                <View style={styles.icon}>
                    <TaliKasih style={styles.icon} />
                    <Text style={styles.title}>
                        <Text style={styles.textBold}>Tali</Text>
                        <Text>Kasih</Text>
                    </Text>
                </View>
                <TextInput onChangeText={(text) => setRegisterForm({...registerForm, name: text})} theme={generalStyles.input} placeholder="Name" />
                <HelperText type="error" visible={errors.name !== ''}>{errors.name}</HelperText>

                <TextInput onChangeText={(text) => setRegisterForm({...registerForm, email: text})} theme={generalStyles.input} placeholder="Email" />
                <HelperText type="error" visible={errors.email !== ''}>{errors.email}</HelperText>

                <TextInput onChangeText={(text) => setRegisterForm({...registerForm, password: text})} theme={generalStyles.input} secureTextEntry={true} placeholder="Password" />
                <HelperText type="error" visible={errors.password !== ''}>{errors.password}</HelperText>


                <TextInput onChangeText={(text) => setRegisterForm({...registerForm, confirm_password: text})} theme={generalStyles.input} secureTextEntry={true} placeholder="Confirm password" />
                <HelperText type="error" visible={errors.confirm_password !== ''}>{errors.confirm_password}</HelperText>

                <View style={{ marginTop: 16 }}>
                    <PrimaryButton onPress={onSubmit} isDisabled={isLoading} title="Register" />
                </View>
             </View>
        </ScrollView>
    )
}


const styles = StyleSheet.create({
    root: {
        flex: 1,
    },
    backButton: {
        position: 'absolute',
        justifyContent: "center"
    },
    continueGoogle: {
        marginTop: 26,
        padding: 16,
        // position: 'absolute',
        alignSelf: 'center',
        justifyContent: "center",
        bottom: 0
    },
    icon: {
        alignSelf: "center"
    },
    forgotPassword: {
        alignSelf: "flex-end",
        marginVertical: 8,
        textDecorationLine: 'underline'
    },
    container: {
        marginTop: 100,
        marginHorizontal: 16,
        flex: 1,
        alignSelf: 'stretch',
        alignItems: 'stretch',
        justifyContent: 'center'
    },
    content: {
        marginTop: 80,
        marginStart: 16,
        marginEnd: 16
    },
    title: {
        marginTop: 8,
        fontSize: 22,
        color: res.PRIMARY_COLOR
    },
    textNewUser: {
        marginTop: 16,
        textDecorationLine: 'underline'
    },
    textPrimaryColor: {
        color: res.PRIMARY_COLOR
    },
    textBold: {
        fontWeight: "bold"
    },
    form: {
        marginHorizontal: 16,
        flex: 1,
        alignSelf: 'stretch',
        alignItems: 'center',
        justifyContent: 'center'
    }
})