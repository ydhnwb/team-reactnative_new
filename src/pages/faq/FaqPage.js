import React from 'react'
import { View, } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler';
import { Card, Title, Paragraph } from 'react-native-paper';

export default function FaqPage() {
    return (
        <ScrollView>
            <Card style={{margin: 8 }}>
                <Card.Content >
                    <Title>How do I create a campaign?</Title>
                    <Paragraph>Login to your account first, then on main page, choose Create Campaign.
                         Fill the required form and submit your campaign (Make sure you are already fill your bank info).
                    </Paragraph>
                </Card.Content>
            </Card>

            <Card style={{margin: 8 }}>
                <Card.Content >
                    <Title>How do I donate to a campaign?</Title>
                    <Paragraph>Make sure you are logged in. Choose particular campaign that you want donate thenn tap 
                        DONATE. Fill the amount how much you want to give, then submit.
                    </Paragraph>
                </Card.Content>
            </Card>

            <Card style={{margin: 8 }}>
                <Card.Content >
                    <Title>How do I update my campaign progress?</Title>
                    <Paragraph>Make sure you are logged in. Go to your campaign detail (campaign that you are the author), 
                        tap "Update Campaign Progress", fill up the forms then save your changes.
                    </Paragraph>
                </Card.Content>
            </Card>
        </ScrollView>
    )
}