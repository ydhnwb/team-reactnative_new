import React, { useEffect } from 'react'
import { StyleSheet, Text, View, Dimensions } from 'react-native'
import * as res from '../../values/colors';
import Talikasih from '../../assets/talikasih.svg'
import TaliKasihText from '../../assets/TaliKasihText.svg'
import FindText from '../../assets/FindText.svg'
import LinearGradient from 'react-native-linear-gradient'
import { StackActions } from '@react-navigation/native';

export const { width, height } = Dimensions.get('window');


export default function SplashScreen({ navigation }) {

    useEffect(() => {
        setTimeout(() => {
            navigation.dispatch(StackActions.replace('Main'));
        }, 3000);
    });

    return (
        <View>
            <LinearGradient colors={[res.COLOR_SILK, res.COLOR_BABYDUST, res.COLOR_DUST]} style={{
                position: "absolute",
                width: width, 
                height: height,
            }}>
                <View style={{ alignItems: "center", justifyContent: "center", flex: 1, marginTop: -10, position: "relative" }}>
                    <Talikasih width={48} height={48} />
                    <TaliKasihText width={width*0.5} height={40} />
                    <FindText width={width*0.5} height={15} />
                </View>
            </LinearGradient>
        </View>
    )
}