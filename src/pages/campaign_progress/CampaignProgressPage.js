import React, { useEffect, useState } from 'react'
import { FlatList, View } from 'react-native'
import * as CampaignService from './../../service/CampaignService';
import { Button } from 'react-native-paper';
import { FundraiserUpdateItem } from './../../components/FundraiserUpdateItem';
import { ScrollView } from 'react-native-gesture-handler';


export function CampaignProgressPage({ navigation, route }) {
    const [currentPage, setCurrenPage] = useState(1)
    const [currentUpdates, setCurrentUpdates] = useState({})
    const [isLoading, setIsLoading] = useState(false)

    const loadMore = () => {
        if(!isLoading){
            if (Object.keys(currentUpdates).length !== 0) {
                const p = currentPage + 1
                fetchCampaignLog(p)
            }
        }

    }

    const fetchCampaignLog = async (page = 1) => {
        setIsLoading(true)
        const res = await CampaignService.getCampaignLog(route.params, page)
        setIsLoading(false)
        if (res.status === 200 && res.data.success) {
            if (Object.keys(currentUpdates).length !== 0) {
                setCurrenPage(currentPage + 1)
                setCurrentUpdates({
                    ...currentUpdates,
                    total_page: res.data.total_page,
                    Campaign_Logs: currentUpdates.Campaign_Logs.concat(res.data.Campaign_Logs)
                })
            } else {
                setCurrenPage(1)
                setCurrentUpdates(res.data)
            }
        }
    }

    useEffect(() => {
        fetchCampaignLog()
    }, [])

    return (
        <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1, paddingHorizontal: 16, paddingVertical: 20, backgroundColor: 'white' }}>
            <FlatList
                showsVerticalScrollIndicator={false}
                scrollEnabled={false}
                style={{ marginTop: 16, marginBottom: 16 }}
                keyExtractor={(item) => item.id.toString()}
                data={currentUpdates.Campaign_Logs}
                renderItem={({ item }) => {
                    return (
                        <FundraiserUpdateItem item={item} />
                    )
                }}
            />
            {
                Object.keys(currentUpdates).length !== 0 ?
                    currentPage !== currentUpdates.total_page ?

                        <Button mode="outlined" style={{ alignSelf: "center", marginBottom: 20 }} onPress={loadMore}>
                            {
                                isLoading ? "LOADING..." : "SHOW MORE"
                            }
                        </Button>
                        : null
                    :
                    null
            }
        </ScrollView>
    )
}