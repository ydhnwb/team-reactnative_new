import React, { useEffect, useState } from 'react';
import { ActivityIndicator, View } from 'react-native';
import { MyDonationItem } from '../../components/MyDonationItem';
import { FlatList } from 'react-native-gesture-handler';
import * as CampaignService from './../../service/CampaignService';
import { useSelector } from 'react-redux';
import * as res from './../../values/colors';



export function MyDonationPage() {
    const [isLoading, setIsLoading] = useState(false)
    const userState = useSelector(state => state.userReducer)
    const [donations, setDonations] = useState([])

    const fetchMyDonations = async () => {
        setIsLoading(true)
        const res = await CampaignService.allMyDonations(userState.token)
        setIsLoading(false)
        console.log(res)
        if (res.status === 200 && res.data.Success) {
            setDonations(res.data.Result)
        }
    }


    useEffect(() => {
        fetchMyDonations()
    }, [])

    return (
        <View style={{ flex: 1 }}>
            {
                isLoading ?
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <ActivityIndicator size="large" color={res.PRIMARY_COLOR} />
                    </View>

                    :

                    <FlatList
                        style={{ padding: 8, marginBottom: 8 }}
                        keyExtractor={(item) => item.id.toString()}
                        data={donations}
                        renderItem={({ item }) => {
                            return <MyDonationItem donation={item} />
                        }}
                    />
            }

        </View>
    )
}