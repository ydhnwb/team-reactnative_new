import React, { useEffect, useRef, useState } from 'react';
import * as utils from './../../utils/utils';
import * as CampaignService from './../../service/CampaignService';
import { Text, View, ImageBackground, StyleSheet, Image, Alert, Dimensions } from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { TextInput, HelperText, Portal, Button, Modal } from 'react-native-paper';
import { generalStyles } from './../../values/styles';
import { PrimaryButton } from '../../components/PrimaryButton';
import DropDownPicker from 'react-native-dropdown-picker';
import { useDispatch, useSelector } from 'react-redux';
import RNDateTimePicker from '@react-native-community/datetimepicker';
import ImagePicker from 'react-native-image-picker';
import { RichEditor, RichToolbar } from 'react-native-pell-rich-editor';
import { LabelText } from './../../components/LabelText';
import * as actions from '../../utils/actions';
import * as res from './../../values/colors';
import { BackHandler } from 'react-native'

const dim = Dimensions.get('window')
const toolbarActions = ['bold', 'italic', 'unorderedList', 'orderedList'];

export function EditCampaignPage({ navigation, route }) {
    const [image, setImage] = useState({})
    const campaignState = useSelector(state => state.campaignReducer)
    const userState = useSelector(state => state.userReducer)
    const [campaignForm, setCampaignForm] = useState({ ...route.params })
    const richText = useRef()
    const [isModalActive, setIsModalActive] = useState(false)
    const dispatch = useDispatch()
    const [editorStyle, setEditorStyle] = useState({ height: 80 })

    const [showDatePicker, setShowDatePicker] = useState(false)
    const [isLoading, setIsLoading] = useState(false)

    const [categories, setCategories] = useState([])
    const [campainErrors, setCampaignErrors] = useState({ title: '', goal: '', due_date: '', bank_account: '', story: '' })


    const showUpDatePicker = () => {
        if (campaignForm.due_date !== '') {
            askDateAction()
        } else {
            setShowDatePicker(true)
        }
    }

    const onChangeTextGoal = (value) => {
        const newGoal = utils.formatRupiah(value)
        setCampaignForm({ ...campaignForm, goal: newGoal })
    }

    const onChangeDate = (event, selectedDate) => {
        const currentDate = selectedDate || campaignForm.due_date;
        setShowDatePicker(Platform.OS === 'ios');
        setCampaignForm({ ...campaignForm, due_date: currentDate.toISOString().split('T')[0] });
    }

    const pickAnImage = () => {
        const options = {
            noData: true,
        }
        ImagePicker.launchImageLibrary(options, response => {
            try {
                console.log(response)
                if (response.uri) {
                    setImage(response)
                }
            } catch (e) {
                console.log(e)
            }
        })
    }

    const validate = () => {
        if (Object.keys(campaignForm.header_img).length === 0) {
            showDialog("Info", "Please select an image first")
            return false
        }
        if (campaignForm.title.toString().trim().length <= 30) {
            setCampaignErrors({ ...campainErrors, title: 'Title at least 30 character', goal: '', due_date: '', bank_account: '', story: '' })
            return false
        }

        if (campaignForm.CategoryId === null) {
            setCampaignErrors({ ...campainErrors, title: '', goal: '', due_date: '', bank_account: '', story: '' })
            showDialog("Info", "Please select a category first")
            return false
        }
        if (campaignForm.goal.trim().length === 0) {
            setCampaignErrors({ ...campainErrors, title: '', goal: 'Please fill the goal first', due_date: '', bank_account: '', story: '' })
            return false
        }
        if (isNaN(parseInt(utils.clearDots(campaignForm.goal.trim())))) {
            setCampaignErrors({ ...campainErrors, title: '', goal: 'Please fill the goal with correct value', due_date: '', bank_account: '', story: '' })
            return false
        } else {
            if (parseInt(utils.clearDots(campaignForm.goal.trim())) <= 0) {
                setCampaignErrors({ ...campainErrors, title: '', goal: 'Please fill the goal greater than zero', due_date: '', bank_account: '', story: '' })
                return false
            }
        }

        if (campaignForm.story.trim().length <= 30) {
            setCampaignErrors({ ...campainErrors, title: '', goal: '', due_date: '', bank_account: '', story: '' })
            showDialog('Info', 'Please write a story with at least 30 characters')
            return false
        }
        setCampaignErrors({ ...campainErrors, title: '', goal: '', due_date: '', bank_account: '', story: '' })
        return true
    }


    const askDateAction = () => {
        Alert.alert("Choose", "What do you want to do with the date?",
            [
                { text: "Change date", onPress: () => setShowDatePicker(true) },
                { text: "Clear date", onPress: () => setCampaignForm({ ...campaignForm, due_date: '' }) }
            ],
            { cancelable: true }
        );
    }

    const showDialog = (title, message) =>
        Alert.alert(title, message,
            [
                { text: "OK", onPress: () => console.log("OK Pressed") }
            ],
            { cancelable: true }
        );

    const editorInitializedCallback = () => {
        richText.current?.registerToolbar((items) => {
            console.log('Toolbar click, selected items (insert end callback):', items);
        });
    }

    const handleHeightChange = (height) => setEditorStyle({ ...editorStyle, height: height })
    const handleChange = (html) => setCampaignForm({ ...campaignForm, story: html })

    const updateCampaignPic = async () => {
        setIsLoading(true)
        const res = await CampaignService.updateCampaignPicture(userState.token, campaignForm.id, image)
        console.log(res)
        setIsLoading(false)
        if (res.status === 200 && res.data.success) {
            navigation.goBack()
        } else {
            showDialog('Failed', "Failed to change the campaign picture")
        }

    }

    const onSubmit = async () => {
        if (validate()) {
            const payload = { ...campaignForm }
            console.log(payload.due_date)
            if (payload.due_date == '') {
                console.log("is true")
                payload['due_date'] = null
            }
            setIsLoading(true)
            console.log(JSON.stringify(payload))
            const res = await CampaignService.updateCampaign(userState.token, payload)
            setIsLoading(false)
            if (res.status === 200) {
                if (Object.keys(image).length !== 0) {
                    updateCampaignPic()
                } else {
                    navigation.goBack()
                }
            } else {
                showDialog("Failed", "Failed when updating campaign data")
            }
        } else {
            showDialog("Failed", "Failed when updating campaign data")
        }
    }


    useEffect(() => {
        setCategories(campaignState.categories)
    }, [])

    return (
        <View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'white' }}>
            {
                true ?
                    <Portal>
                        <Modal visible={isModalActive} onDismiss={() => setIsModalActive(false)}>
                            <View>
                                <ScrollView>
                                    <RichEditor
                                        initialFocus={false}
                                        ref={richText}
                                        initialContentHTML={campaignForm.story}
                                        // value={campaignForm.story}
                                        containerStyle={{ height: 50, maxHeight: editorStyle.height }}
                                        placeholder={'Please input content'}
                                        editorInitializedCallback={editorInitializedCallback}
                                        onChange={handleChange}
                                        onHeightChange={handleHeightChange}
                                    />
                                </ScrollView>

                                <View style={styles.ctx}>
                                    <RichToolbar
                                        style={[styles.richBar]}
                                        editor={richText}
                                        iconTint={'#8b8b8b'}
                                        selectedIconTint={'#2095F2'}
                                        disabledIconTint={'#8b8b8b'}
                                        actions={[...toolbarActions,]}
                                    />
                                </View>

                            </View>
                        </Modal>
                    </Portal>
                    : null
            }
            <ScrollView>
                <View style={styles.root}>
                    {
                        showDatePicker ?
                            <RNDateTimePicker
                                testID="dateTimePicker"
                                value={campaignForm.due_date === '' ? new Date((new Date()).toISOString().split('T')[0]) : new Date(campaignForm.due_date)}
                                mode='date'
                                is24Hour={true}
                                display="default"
                                onChange={onChangeDate}
                            />
                            : null
                    }

                    <Image style={{ height: 170 }} source={{
                        uri: Object.keys(image).length === 0 ? campaignForm.header_img : image.uri
                    }} />

                    {
                        Object.keys(campaignForm.header_img).length === 0 ? null :
                            <Text onPress={pickAnImage} style={styles.changeBackgroundText}>Change background</Text>

                    }

                    <LabelText title="Title" />
                    <TextInput value={campaignForm.title} onChangeText={(text) => setCampaignForm({ ...campaignForm, title: text })} theme={generalStyles.input} placeholder="Title" />
                    <HelperText type="error" visible={campainErrors.title !== ''}>{campainErrors.title}</HelperText>

                    <View style={{ marginTop: 8, marginBottom: 16 }}>
                        <View style={{ marginBottom: 8 }}>
                            <LabelText title="Category" />
                        </View>
                        <DropDownPicker
                            items={categories}
                            containerStyle={{ height: 40 }}
                            itemStyle={{ justifyContent: 'flex-start' }}
                            onChangeItem={item => setCampaignForm({ ...campaignForm, CategoryId: item.id })}
                        />
                    </View>


                    <LabelText title="Goal" />
                    <TextInput contextMenuHidden={true} keyboardType="number-pad" value={campaignForm.goal} onChangeText={(text) => onChangeTextGoal(text)} theme={generalStyles.input} placeholder="Goal" />
                    <HelperText type="error" visible={campainErrors.goal !== ''}>{campainErrors.goal}</HelperText>

                    <LabelText title="Due date" />
                    <TouchableOpacity onPress={() => showUpDatePicker()}>
                        <View pointerEvents="none">
                            <TextInput value={campaignForm.due_date} focusable={false} theme={generalStyles.input} placeholder="Due date (optional)" />
                        </View>
                    </TouchableOpacity>
                    <HelperText type="error" visible={campainErrors.due_date !== ''}>{campainErrors.due_date}</HelperText>
                    <LabelText title="Story" />
                    <Text onPress={() => setIsModalActive(true)} style={styles.editStoryText}>Edit story</Text>


                    <View style={{ marginTop: 56 }}>
                        <PrimaryButton onPress={onSubmit} isDisabled={isLoading} title="Update campaign" />
                    </View>

                </View>
            </ScrollView>

        </View>


    )
}


const styles = StyleSheet.create({
    root: {
        marginHorizontal: 16,
        marginBottom: 16
    },
    img: {
        flex: 1,
        // height: screenHeight * 0.27,
        height: 170,
        margin: 16,
    },
    editStoryText: {
        fontFamily: "Nunito-Regular",
        marginTop: 16,
        color: res.PRIMARY_COLOR,
        textDecorationLine: 'underline'
    },
    changeBackgroundText: {
        fontFamily: "Nunito-Regular",
        marginTop: 16,
        alignSelf: 'center',
        color: res.PRIMARY_COLOR,
        textDecorationLine: 'underline'
    },
    containerText: {
        backgroundColor: 'rgba(0,0,0,0.3)',
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 16
    },
    titleText: {
        fontSize: 16,
        color: 'white'
    },
    subtitleText: {
        marginStart: 4,
        fontSize: 12,
        color: 'white'
    },
    notLoggedInView: {
        alignItems: 'center',
        justifyContent: "center",
        alignContent: 'center'

    },
    ctx: {
        backgroundColor: 'white',
        position: "absolute",
        bottom: 0,
        alignSelf: 'center',
        alignContent: 'stretch',
        alignItems: 'stretch'

    },

    richBar: {
        height: 50,
        backgroundColor: 'white',
    },
    rich: {
        minHeight: 60,
        flex: 1
    },
    tib: {
        textAlign: 'center',
        color: '#515156',
    },
})
