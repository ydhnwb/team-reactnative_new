import React, { useState } from 'react';
import { View, Text, StyleSheet, Alert } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { generalStyles } from '../../values/styles';
import { RadioButton, TextInput, Button } from 'react-native-paper';
import * as utils from './../../utils/utils';
import * as CampaignService from './../../service/CampaignService';
import * as res from './../../values/colors';
import { useSelector } from 'react-redux';

export function UpdateCampaignProgressPage({ navigation, route }) {
    const userState = useSelector(state => state.userReducer)
    const [radioSelect, setRadioSelect] = useState(2)
    const [log, setLog] = useState({
        UserId:userState.user.id,
        content: '',
        StatusId: 2,
        date: (new Date()).toISOString().split('T')[0],
        ammount: ''
    })

    const onChangeTextAmount = (amount) => {
        const newAmount = utils.formatRupiah(amount)
        setLog({...log, ammount: newAmount})
    }

    const onRadioChange = (statusId) => {
        setLog({...log, ammount: '', StatusId: statusId})
        setRadioSelect(statusId)
    }

    const validate = () => {
        if(parseInt(utils.clearDots(log.ammount)) <= 0){
            showDialog("Info", "Please enter a valid amount")
            return false
        }
        if(log.content.toString().trim() == ''){
            showDialog("Info", "Please write your support :)")
            return false
        }
        return true
    }

    const showDialog = (title, message) => Alert.alert(title, message,
        [
            { text: "OK", onPress: () => console.log("OK Pressed") }
        ],
        { cancelable: true }
    );

    const onSubmit = async () => {
        if(validate()){
            const res = await CampaignService.addCampaignLog(userState.token, route.params, log)
            console.log(res)
            if(res.status === 201 && res.data.success){
                navigation.goBack()
            }else{
                showDialog("Failed", "Cannot update progress your campaign")
            }
        }

    }

    return (
        <View style={styles.root}>
            <ScrollView style={styles.scroll}>
                <View style={styles.radio}>
                    <RadioButton color={res.PRIMARY_COLOR} value={2} status={radioSelect === 2 ? 'checked' : 'unchecked'} onPress={() => onRadioChange(2)} />
                    <Text style={styles.radioText}>Recipient update</Text>
                </View>
                <View style={styles.radio}>
                    <RadioButton color={res.PRIMARY_COLOR} value={1} status={radioSelect === 1 ? 'checked' : 'unchecked'} onPress={() => onRadioChange(1)} />
                    <Text style={styles.radioText}>Fund withdrawal</Text>
                </View>

                {
                    radioSelect === 1 ?
                    <TextInput value={log.ammount} contextMenuHidden={true} keyboardType="number-pad" onChangeText={(text) => onChangeTextAmount(text)} theme={generalStyles.input} placeholder="Amount" />
                    :
                    null

                }
                <TextInput onChangeText={(t) => setLog({...log, content: t})} theme={generalStyles.input} placeholder="Tell your story" multiline={true} numberOfLines={6} />
            </ScrollView>
            <View style={styles.bottomButton}>
                <Button color='white' mode="text" style={{ alignSelf: "stretch" }} onPress={onSubmit}>
                    UPDATE PROGRESS
                </Button>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    root: {
        flex: 1
    },
    scroll: {
        paddingHorizontal: 16,
        paddingTop: 16,
        marginBottom: 40
    },
    radio: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    radioText: {
        marginStart: 8,
        fontWeight: "700",
        fontSize: 18
    },
    bottomButton: {
        backgroundColor: res.COLOR_ROSE,
        padding: 4,
        position: 'absolute',
        justifyContent: "center",
        bottom: 0,
        left: 0,
        right: 0,
        alignItems: 'center',
    },
})