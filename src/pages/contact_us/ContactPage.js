import React from 'react'
import { StyleSheet, View, Image, Platform, Text } from "react-native";
import MapboxGL from "@react-native-mapbox-gl/maps";
import FontAwesome from 'react-native-vector-icons/FontAwesome'

MapboxGL.setAccessToken("pk.eyJ1IjoieWRobndiIiwiYSI6ImNraG9pamx4dzAweWMycW8ybGxwazRydW4ifQ.4_s9_9EBEug_YrpcXaB2fA");

export default function ContactPage() {
    return (
        <View style={{ flex: 1 }}>
            <View style={{ borderRadius: 8, backgroundColor: 'white', padding: 16 }}>
                <Text>Jl. Jl. Hang Lekiu No.KM 2, Sambau, Kecamatan Nongsa, Kota Batam, Kepulauan Riau 29465</Text>
            </View>
            <MapboxGL.MapView
                styleURL={MapboxGL.StyleURL.Street}
                zoomLevel={16}
                centerCoordinate={[104.101709,1.185663]}
                style={{ flex: 1 }}>
                <MapboxGL.Camera
                    zoomLevel={16}
                    centerCoordinate={[104.101709,1.185663]}
                    animationMode={'flyTo'}
                    animationDuration={0}
                >
                </MapboxGL.Camera>
                <MapboxGL.PointAnnotation             
                    coordinate={[104.101709,1.185663]} id="Test" />
       
            </MapboxGL.MapView>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
      alignItems: 'center',
      width: 64,
      backgroundColor: 'transparent',
      height: 64,
    },
    textContainer: {
      backgroundColor: 'white',
      borderRadius: 10,
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center',
    },
    text: {
      textAlign: 'center',
      paddingHorizontal: 10,
      flex: 1,
    },
    icon: {
      paddingTop: 16,
    },
   });