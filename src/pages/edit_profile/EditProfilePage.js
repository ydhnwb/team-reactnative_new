import React, { useEffect, useState } from 'react';
import { ScrollView } from 'react-native-gesture-handler';
import { Text, View, Alert, StyleSheet, Image } from 'react-native';
import { TextInput, HelperText, } from 'react-native-paper';
import * as res from '../../values/colors';
import * as validators from './../../utils/validators';
import { generalStyles } from '../../values/styles';
import { LabelText } from '../../components/LabelText';
import { useDispatch, useSelector } from 'react-redux';
import { BottomButton } from './../../components/BottomButton';
import ImagePicker from 'react-native-image-picker';
import * as UserService from './../../service/UserService';
import * as actions from './../../utils/actions';
import * as storage from './../../utils/SharedPref';
import * as utils from './../../utils/utils';
import * as urls from './../../service/urls';

export function EditProfilePage({ navigation }) {
    const dispatch = useDispatch()
    const userState = useSelector(state => state.userReducer)
    const [image, setImage] = useState({})
    const [isExpanded, setIsExpanded] = useState(false)
    const [userErrors, setUserErrors] = useState({
        name: '',
        email: '',
        password: '',
        confirm_password: ''
    })
    const [userForm, setUserForm] = useState({ name: '', email: '' })
    const [passwordForm, setPasswordForm] = useState({ password: '', confirm_password: '' })
    const [bankForm, setBankForm] = useState({ bank_name: '', bank_account_number: '' })
    const [bankErrors, setBankErrors] = useState({ bank_name: '', bank_account: '' })

    const validate = () => {
        if (userForm.name.trim().length === 0) {
            setUserErrors({ ...userErrors, name: 'Name cannot be empty', email: "", password: '', confirm_password: '' })
            return false
        }
        if (!validators.isValidEmail(userForm.email.trim())) {
            setUserErrors({ ...userErrors, email: 'Email is not valid', name: '', password: '', confirm_password: '' })
            return false
        }
        if (passwordForm.password.trim().length !== 0) {
            if (!validators.isValidPassword(passwordForm.password)) {
                setUserErrors({ ...userErrors, email: '', name: '', password: 'Password must contains at least 6 chars with letter and number', confirm_password: '' })
                return false
            }
            if (passwordForm.confirm_password.trim() !== passwordForm.password) {
                setUserErrors({ ...userErrors, email: '', name: '', password: '', confirm_password: 'Confirm password is not equal' })
                return false
            }
        }
        setUserErrors({ ...userErrors, name: "", email: "", password: '', confirm_password: '' })
        return true
    }

    const pickAnImage = () => {
        const options = {
            noData: true,
        }
        ImagePicker.launchImageLibrary(options, response => {
            try {
                console.log(response)
                if (response.uri) {
                    setImage(response)
                }
            } catch (e) {
                console.log(e)
            }

        })
    }

    const updateProfilePicture = async () => {
        const res = await UserService.profilePictureUpdate(userState.token, image)
        if (res.status === 200 && res.data.success) {
            fetchProfile(userState.token)
        } else {
            showDialog("Failed", "Cannot change your profile picture. Try again later")
        }
    }


    const checkBankStatus = async (token, current_bank_acc) => {
        const res = await UserService.profile(token)
        if (res.status === 200 && (!res.data.hasOwnProperty("success") || res.data.success)) {
            if (current_bank_acc == null && res.data.user.bank_account != null) {
                return false
            }
            return true
        }
        return false
    }
    const fetchProfile = async (token) => {
        const res = await UserService.profile(token)
        if (res.status === 200 && (!res.data.hasOwnProperty("success") || res.data.success)) {
            await storage.saveUserData(res.data.user)
            await storage.setToken(token)
            dispatch({
                type: actions.FETCH_TOKEN, payload: {
                    token: token,
                    user: res.data.user
                }
            })
            navigation.goBack()
        } else {
            showDialog("Failed", "Cannot update your data")
        }
    }

    const doUpdate = async () => {
        if (validate()) {
            if (validateBankForm()) {
                const payload = {
                    name: userForm.name,
                    bank_name: bankForm.bank_name == '' ? null : bankForm.bank_name,
                    bank_account: bankForm.bank_account == '' ? null : bankForm.bank_account
                }

                const isOkayToEmptyBank = await checkBankStatus(userState.token, payload.bank_account)
                if (isOkayToEmptyBank) {
                    if (userForm.email !== userState.user.email) { payload['email'] = userForm.email }
                    if (passwordForm.password !== '') { payload['password'] = passwordForm.password }
                    const res = await UserService.profileUpdate(userState.token, payload)
                    if (res.status === 200 && (!res.data.hasOwnProperty("success") || res.data.success)) {
                        if (Object.keys(image).length !== 0) {
                            updateProfilePicture()
                        } else {
                            fetchProfile(userState.token)
                        }
                    } else {
                        const message = res.data !== null ? res.data.message : "Cannot update your data. Try again later"
                        showDialog("Failed", message)
                    }
                } else {
                    showDialog("Failed", "You cannot empty your bank info")
                }
            }

        }
    }

    const validateBankForm = () => {
        setBankErrors({ ...bankErrors, bank_name: '', bank_account: '' })
        if (bankForm.bank_name !== '' && bankForm.bank_account !== '') {
            return true
        } else if (bankForm.bank_name == '' && bankForm.bank_account == '') {
            return true
        }
        setBankErrors({ ...bankErrors, bank_name: 'If you want to fill, fill these two form is mandatory', bank_account: 'If you want to fill, fill these two form is mandatory' })
        return false
    }

    const showDialog = (title, message) =>
        Alert.alert(title, message,
            [
                { text: "OK", onPress: () => console.log("OK Pressed") }
            ],
            { cancelable: true }
        );

    useEffect(() => {
        setUserForm({ ...userForm, name: userState.user.name, email: userState.user.email })
        setBankForm({ ...bankForm, bank_name: userState.user.bank_name == null ? '' : userState.user.bank_name, bank_account: userState.user.bank_account == null ? '' : userState.user.bank_account.toString() })
    }, [])

    return (
        <View style={{ flex: 1, backgroundColor: 'white' }}>
            <View style={styles.bottomButton}>
                <BottomButton title="SAVE CHANGES" onTap={doUpdate} />
            </View>
            <ScrollView style={styles.root}>
                <View>
                    <Image style={styles.img} source={{
                        uri: Object.keys(image).length !== 0 ? image.uri :
                        
                        userState.user.photo == null ? 
                         urls.DEF_USER_IMAGE :  userState.user.photo
                    }} />
                    <Text onPress={() => pickAnImage()} style={styles.editProfileText}>Change profile picture</Text>

                    <LabelText title="Name" />
                    <TextInput value={userForm.name} onChangeText={(text) => setUserForm({ ...userForm, name: text })} style={styles.input} theme={generalStyles.input} placeholder="Name" />
                    <HelperText type="error" visible={userErrors.name !== ''}>{userErrors.name}</HelperText>

                    <LabelText title="Email" />
                    <TextInput disabled={true} value={userForm.email} onChangeText={(text) => setUserForm({ ...userForm, email: text })} style={styles.input} theme={generalStyles.input} placeholder="Email" />
                    <HelperText type="error" visible={userErrors.email !== ''}>{userErrors.email}</HelperText>

                    {
                        isExpanded ?
                            <View>
                                <LabelText title="New password" />
                                <TextInput secureTextEntry={true} onChangeText={(text) => setPasswordForm({ ...passwordForm, password: text })} style={styles.input} theme={generalStyles.input} placeholder="New password" />
                                <HelperText type="error" visible={userErrors.password !== ''}>{userErrors.password}</HelperText>

                                <LabelText title="Confirm new password" />
                                <TextInput secureTextEntry={true} onChangeText={(text) => setPasswordForm({ ...passwordForm, confirm_password: text })} style={styles.input} theme={generalStyles.input} placeholder="Confirm password" />
                                <HelperText type="error" visible={userErrors.confirm_password !== ''}>{userErrors.confirm_password}</HelperText>
                            </View>
                            :
                            <Text onPress={() => setIsExpanded(!isExpanded)} style={styles.resetPassword}>Reset password</Text>

                    }

                    <LabelText title="Bank name" />
                    <TextInput value={bankForm.bank_name} onChangeText={(t) => setBankForm({ ...bankForm, bank_name: t })} style={styles.input} theme={generalStyles.input} placeholder="Bank name" />
                    <HelperText type="error" visible={bankErrors.bank_name !== ''}>{bankErrors.bank_name}</HelperText>

                    <LabelText title="Bank account number" />
                    <TextInput keyboardType="number-pad" value={bankForm.bank_account} onChangeText={(t) => setBankForm({ ...bankForm, bank_account: t })} style={styles.input} theme={generalStyles.input} placeholder="Bank account number" />
                    <HelperText type="error" visible={bankErrors.bank_account !== ''}>{bankErrors.bank_account}</HelperText>


                </View>
            </ScrollView>
        </View>

    )
}

const styles = StyleSheet.create({
    root: {
        paddingHorizontal: 16,
        marginBottom: 56
    },
    input: {
        marginTop: -8
    },
    img: {
        alignSelf: 'center',
        backgroundColor: 'gray',
        flex: 1,
        width: 150,
        height: 150,
        margin: 16,
    },
    editProfileText: {
        fontFamily: "Nunito-Regular",
        alignSelf: 'center',
        color: res.PRIMARY_COLOR,
        marginBottom: 16,
        textDecorationLine: 'underline'
    },
    profileHelperText: {
        color: 'gray'
    },
    profileText: {
        fontSize: 18,
        fontWeight: "600",
        marginBottom: 16
    },
    resetPassword: {
        alignSelf: 'flex-end',
        fontWeight: "bold",
        textDecorationLine: 'underline'
    },
    bottomButton: {
        backgroundColor: res.COLOR_ROSE,
        padding: 4,
        position: 'absolute',
        justifyContent: "center",
        bottom: 0,
        left: 0,
        right: 0,
        alignItems: 'center',
    },
})