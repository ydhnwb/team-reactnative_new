import React, { useState } from 'react'
import { StyleSheet, Text, View, ScrollView } from 'react-native'
import { BottomButton } from './../../components/BottomButton';
import * as res from './../../values/colors';
import ExploreComponent from '../../components/ExploreComponent'
import DataImport from './../../../Config/DataImport.json';
import { Button, List, Divider } from 'react-native-paper';



export default function FilterAndSortScreen({ navigation, route }) {
    const [categorySelected, setCategorySelected] = useState(1)
    const onSelectedCategory = (category) => {
        if (categorySelected === category.id) {
            setCategorySelected(0)
        } else {
            setCategorySelected(category.id)
        }
    }

    const [selectedSort, setSelectedSort] = useState({
        idx: 0,
        value: 'newest'
    })
    const sortDatas = ["Newest", "Most urgent", "Popular", "Less donation"]
    const sortDatasValue = ['newest', 'urgent', 'popular', 'lessdonate']

    const onSelectSort = (index) => {
        const v = sortDatasValue[index]
        setSelectedSort({ ...selectedSort, idx: index, value: v })
    }

    const onFilterSubmit = () => {
        route.params.onFilterSelect(categorySelected, selectedSort.value)
        navigation.goBack()
    }

    const sortComponent = (s, idx) => {
        return (
            <View>
                <List.Item
                    key={idx}
                    style={idx === selectedSort.idx ? styles.sortItemSelected : styles.sortItem}
                    onPress={() => onSelectSort(idx)}
                    title={s}
                    right={() => {
                        const icon = idx === selectedSort.idx ? <List.Icon style={{ width: 18, height: 18 }} color={res.PRIMARY_COLOR} icon="check" /> : null
                        return icon
                    }}
                />
                <Divider />
            </View>
        )
    }


    return (
        <View style={{ flex: 1 }}>
            <ScrollView style={{ backgroundColor: 'white', padding: 10 }}>
                <Text style={styles.textHelper}>Filter by category</Text>
                <ExploreComponent saveState={true} onSelectedCategory={onSelectedCategory} />
                <Text style={{ marginTop: 15, color: 'gray', fontFamily: "Nunito-Regular", }}>Sort by</Text>
                <List.Section>
                    {
                        sortDatas.map((sortedItem, index) => {
                            return sortComponent(sortedItem, index)
                        })
                    }
                </List.Section>
            </ScrollView>
            <View style={styles.bottomButton}>
                <Button color="white" mode="text" style={{ alignSelf: "stretch" }} onPress={onFilterSubmit}>
                    FILTER
                </Button>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    bottomButton: {
        backgroundColor: res.COLOR_ROSE,
        padding: 4,
        position: 'absolute',
        justifyContent: "center",
        bottom: 0,
        left: 0,
        right: 0,
        alignItems: 'center',
    },
    textHelper: {
        fontFamily: "Nunito-Regular",
        marginVertical: 5,
        color: 'gray'
    },
    sortItem: {
    },
    sortItemSelected: {
        backgroundColor: res.COLOR_DUST
    }
})
