import React from 'react'
import { View, } from 'react-native'
import { Card, Title, Paragraph } from 'react-native-paper';

export default function AboutPage() {
    return (
        <View>
            <Card style={{margin: 8 }}>
                <Card.Content>
                    <Title>Tali Kasih v.0.0.1</Title>
                    <Paragraph>Menjalin Kasih Antara Kita</Paragraph>
                </Card.Content>

            </Card>
        </View>
    )
}