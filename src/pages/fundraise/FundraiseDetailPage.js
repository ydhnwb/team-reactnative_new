import React, { useEffect, useRef, useState } from 'react';
import { Text, StyleSheet, View, FlatList, Dimensions } from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { CommentSection } from '../../components/CommentSection';
import { DonationUpdate } from '../../components/DonationUpdate';
import { FundraiseCover } from '../../components/FundraiseCover';
import { FundraiseInfo } from '../../components/FundraiseInfo';
import { FundraiseUpdate } from '../../components/FundraiseUpdate';
import { CampaignItem } from '../../components/CampaignItem';
import * as res from './../../values/colors';
import { BottomButtonUpdateProgress } from '../../components/BottomButtonUpdateProgress';
import * as CampaignService from './../../service/CampaignService';
import HTMLView from 'react-native-htmlview';
import * as utils from '../../utils/utils';
import { ActivityIndicator, IconButton, TextInput } from 'react-native-paper';
import { LabelText } from '../../components/LabelText';
import LinearGradient from 'react-native-linear-gradient';
import { useSelector } from 'react-redux';
import RBSheet from "react-native-raw-bottom-sheet";
import Clipboard from '@react-native-community/clipboard';
import { generalStyles } from './../../values/styles';
import { PrimaryButton } from './../../components/PrimaryButton';

export function FundraiseDetailPage({ navigation, route }) {
    const refRBSheet = useRef();
    const refRbSheetCampaign = useRef()
    const userState = useSelector(state => state.userReducer)
    const [isStoryExpanded, setIsStoryExpanded] = useState(false)
    const [currentCampaign, setCurrentCampaign] = useState({})
    const [currentComments, setCurrentComments] = useState([])
    const [currentLogs, setCurrentLogs] = useState([])
    const [currentDonations, setCurrentDonations] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    const [relatedCampaign, setRelatedCampaign] = useState([])

    const goToCampaignDetail = (campaign) => navigation.push("Fundraiser", {
        campaign_id: campaign.id,
        CategoryId: route.params.CategoryId
    })

    const goToEditCampaignPage = () => {
        navigation.navigate("Edit campaign", {
            id: currentCampaign.id,
            title: currentCampaign.title,
            goal: utils.formatRupiah(currentCampaign.goal.toString()),
            header_img: currentCampaign.header_img,
            story: currentCampaign.story,
            bankAccount: userState.user.bank_account,
            due_date: currentCampaign.due_date == null ? '' : currentCampaign.due_date.split("T")[0],
            CategoryId: currentCampaign.Category.id,
            category: {
                id: currentCampaign.Category.id,
                label: currentCampaign.Category.name,
                value: currentCampaign.Category.name.toLowerCase()
            }
        })
    }

    const editCampaign = () => {
        refRbSheetCampaign.current.open()
    }

    const fetchCurrentCampaign = async () => {
        const res = await CampaignService.getCampaignById(route.params.campaign_id)
        if (res.status === 200 && res.data.success) {
            setCurrentCampaign(res.data.found)
        } else {
            console.log("Failed when get campaign by id")
        }
    }

    const fetchRelatedCampaign = async () => {
        const res = await CampaignService.allCampaignByCategory(route.params.CategoryId)
        console.log(res)
        if (res.status === 200) {
            setRelatedCampaign(res.data.document)
        }
    }

    const fetchCurrentLog = async () => {
        const res = await CampaignService.getCampaignLog(route.params.campaign_id)
        if (res.status === 200 && res.data.success) {
            setCurrentLogs(res.data.Campaign_Logs)
        } else {
            console.log("Log is empty")
        }
    }

    const fetchCurrentComments = async () => {
        const res = await CampaignService.getComments(route.params.campaign_id)
        console.log(res)
        if (res.status === 200 && res.data.success) {
            setCurrentComments(res.data.comments)
        }
    }

    const fetchCampaignDonation = async () => {
        const res = await CampaignService.getCampaignDonation(route.params.campaign_id)
        if (res.status === 200) {
            setCurrentDonations(res.data)
        }
    }

    const deleteCampaign = async () => {
        const res = await CampaignService.deleteCampaignById(route.params.campaign_id, userState.token)
        console.log(res)
        if (res.status == 200) {
            navigation.goBack()
        }
    }


    const goToCommentPage = () => navigation.navigate("Comments", currentCampaign.id)
    const goToCampaignDonationPage = () => navigation.navigate("Donations", currentCampaign.id)
    const goToCampaignProgressPage = () => navigation.navigate("Campaign progress", currentCampaign.id)
    const goToUpdateProgressPage = () => navigation.navigate("Update campaign progress", currentCampaign.id)
    const goToDonatePage = () => navigation.navigate("Donation", {
        campaign_id: currentCampaign.id,
        user: currentCampaign.User
    })


    const getData = async () => {
        setIsLoading(true)
        await fetchCurrentCampaign()
        await fetchCurrentLog()
        await fetchCampaignDonation()
        await fetchCurrentComments()
        setIsLoading(false)
        fetchRelatedCampaign()

    }

    useEffect(() => {
        navigation.addListener('focus', () => {
            getData()
        });

    }, [])
    return (
        <View style={{ flex: 1 }}>
            {
                isLoading ?
                    <View style={{ flex: 1, justifyContent: 'center', }}>
                        <ActivityIndicator style={{ alignSelf: 'center' }} size="small" color={res.PRIMARY_COLOR} />
                    </View>
                    :
                    <ScrollView style={{ backgroundColor: 'white' }}>
                        <FundraiseCover header_img_url={currentCampaign === null ? "" : currentCampaign.header_img} />
                        <Text style={styles.titleText}>{currentCampaign.title}</Text>
                        {
                            currentCampaign !== null ?
                                <View style={{ marginHorizontal: 16 }}>
                                    <FundraiseInfo onTap={goToUpdateProgressPage} campaign={currentCampaign} />
                                </View> : null
                        }

                        {
                            Object.keys(currentCampaign).length === 0 ?
                                null : userState.user === null ?
                                    null : Object.keys(currentCampaign.User).length === 0 ?
                                        null : userState.user.id === currentCampaign.User.id ?
                                            <Text onPress={editCampaign} style={styles.manageCampaign}>Manage campaign</Text>
                                            : null


                        }

                        <View style={[styles.descriptionContainer, isStoryExpanded ? null : { maxHeight: 190 }]}>
                            <Text style={styles.theStory}>The story</Text>
                            {
                                currentCampaign !== null ?
                                    <HTMLView style={{ marginHorizontal: 16, }} value={currentCampaign.story} /> : null
                            }
                            {
                                isStoryExpanded ? null :
                                    <LinearGradient
                                        colors={['transparent', 'rgba(255,255,255,1)']}
                                        start={{ x: 0, y: .5 }}
                                        end={{ x: 0, y: 1 }}
                                        style={styles.linearGradientContainer}
                                    />
                            }


                        </View>

                        {
                            isStoryExpanded ? null :
                                <View style={{ alignSelf: 'center' }}>
                                    <Text onPress={() => setIsStoryExpanded(true)} style={styles.readMore}>Read more</Text>
                                </View>

                        }

                        <View style={{ margin: 16 }}>
                            <FundraiseUpdate onTap={goToCampaignProgressPage} logs={currentLogs} />
                        </View>

                        <View style={{ margin: 16 }}>
                            <DonationUpdate onTap={goToCampaignDonationPage} donations={currentDonations} />
                        </View>

                        <View style={{ margin: 16 }}>
                            <CommentSection refreshComment={fetchCurrentComments} campaignId={route.params.campaign_id} onTap={goToCommentPage} comments={currentComments} />
                        </View>

                        <Text style={styles.relatedCampaign}>Related campaign</Text>
                        <FlatList
                            showsHorizontalScrollIndicator={false}
                            marginHorizontal={8}
                            horizontal={true}
                            style={{ marginBottom: 46 }}
                            keyExtractor={(item) => item.id.toString()}
                            data={relatedCampaign}
                            renderItem={({ item }) => {
                                return <CampaignItem campaign={item} small={true} onTap={goToCampaignDetail} />
                            }}
                        />

                    </ScrollView>
            }
            <RBSheet
                ref={refRBSheet}
                closeOnDragDown={false}
                closeOnPressMask={false}
                customStyles={{
                    container: {
                        maxHeight: 200,
                        height: 190,
                        paddingVertical: 16,
                        paddingHorizontal: 8
                    },
                    wrapper: {
                        backgroundColor: "transparent"
                    },
                    draggableIcon: {
                        backgroundColor: "#000"
                    }
                }}
            >
                <View style={{ flexDirection: 'column', justifyContent: "flex-end" }}>
                    <View style={{ flexDirection: "row", justifyContent: "space-between", alignSelf: 'stretch', alignItems: 'center' }}>
                        <Text style={{ fontWeight: 'bold' }}>Help by sharing</Text>
                        <IconButton icon='close' onPress={() => refRBSheet.current.close()} />
                    </View>

                    <TextInput disabled={true} value={`https://production-react-talikasih.herokuapp.com/campaign/details/donate/${route.params.campaign_id}`} theme={generalStyles.input} placeholder="Link" />
                    <View style={{ marginTop: 8 }}>
                        <PrimaryButton onPress={() => {
                            Clipboard.setString(`https://production-react-talikasih.herokuapp.com/campaign/details/donate/${route.params.campaign_id}`)
                            refRBSheet.current.close()

                        }} title="Copy Link" />
                    </View>

                </View>
            </RBSheet>

            <RBSheet
                ref={refRbSheetCampaign}
                closeOnDragDown={false}
                closeOnPressMask={false}
                customStyles={{
                    container: {
                        maxHeight: 220,
                        height: 210,
                        paddingVertical: 16,
                        paddingHorizontal: 8
                    },
                    wrapper: {
                        backgroundColor: "transparent"
                    },
                    draggableIcon: {
                        backgroundColor: "#000"
                    }
                }}
            >
                <View>
                    <View style={{ flexDirection: "row", justifyContent: "space-between", alignSelf: 'stretch', alignItems: 'center' }}>
                        <Text style={{ fontWeight: 'bold' }}>Manage campaign</Text>
                        <IconButton icon='close' onPress={() => refRbSheetCampaign.current.close()} />
                    </View>
                    <View style={{ padding: 8 }}>
                        <ScrollView>
                            <Text onPress={() => {
                                refRbSheetCampaign.current.close()
                                goToEditCampaignPage()

                            }} style={{ padding: 8 }}>Edit</Text>
                            <Text onPress={() => {
                                refRbSheetCampaign.current.close()
                            }} style={{ padding: 8 }}>Close campaign</Text>
                            <Text onPress={() => {
                                refRbSheetCampaign.current.close()
                                deleteCampaign()
                            }} style={{ padding: 8, color: 'red' }}>Delete</Text>



                        </ScrollView>


                    </View>
                </View>
            </RBSheet>

            <View style={styles.bottomButton}>
                {
                    Object.keys(currentCampaign).length === 0 ?
                        null : userState.user === null ?
                            null : Object.keys(currentCampaign.User).length === 0 ?
                                null : userState.user.id === currentCampaign.User.id ?
                                    <BottomButtonUpdateProgress title="UPDATE PROGRESS" onShare={() => refRBSheet.current.open()} onTap={goToUpdateProgressPage} />
                                    :

                                    <BottomButtonUpdateProgress title="DONATE" onTap={goToDonatePage} onShare={() => refRBSheet.current.open()} />

                }
            </View>
        </View>

    )
}

const styles = StyleSheet.create({
    titleText: {
        fontWeight: "bold",
        fontFamily: "Nunito-Regular",
        margin: 16,
        alignSelf: "stretch",
        fontSize: 22,
        color: 'black'
    },
    donationAmountText: {
        fontFamily: "Nunito-regular",
        fontWeight: "700",
        marginHorizontal: 16,
        color: res.COLOR_ROSE,
        fontSize: 20
    },
    subtitleText: {
        fontFamily: "Nunito-Regular",
        color: 'gray',
        marginTop: 8,
        marginBottom: 16,
        marginHorizontal: 16
    },
    manageCampaign: {
        fontFamily: "Nunito-Regular",
        fontSize: 19,
        marginVertical: 16,
        fontWeight: "600",
        textDecorationLine: 'underline',
        alignSelf: 'center'
    },
    theStory: {
        marginTop: 16,
        marginBottom: 16,
        fontSize: 20,
        fontFamily: "Nunito-Regular",
        fontWeight: "bold",
        alignSelf: 'center'
    },
    storyContent: {
        alignSelf: 'center',
        marginHorizontal: 16,
        marginBottom: 16,
    },
    relatedCampaign: {
        margin: 16,
        fontSize: 16,
        fontFamily: "Nunito-Regular",
        fontWeight: 'bold',
        textDecorationLine: 'underline'
    },
    bottomButton: {
        backgroundColor: 'white',
        position: 'absolute',
        justifyContent: "center",
        bottom: 0,
        left: 0,
        right: 0,
        alignItems: 'center',
    },
    descriptionContainer: {
        marginBottom: 26,
        flex: 1,
        overflow: 'hidden',
    },
    descriptionHeading: {
        marginTop: 20,
        fontSize: 21,
        color: '#212121',
    },
    descriptionText: {
        fontSize: 15,
        color: '#6f6f6f',
    },
    linearGradientContainer: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    },
    readMore: {
        fontSize: 18,
        fontFamily: "Nunito-Regular",
        textDecorationLine: 'underline',
        color: res.COLOR_CHOPPER
    }
})