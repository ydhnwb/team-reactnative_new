export const isValidPassword = (password) => {
    const regexPassword = /^(?=[a-zA-Z0-9]*[a-zA-Z])(?=[a-zA-Z0-9]*\d)[a-zA-Z0-9]*$/i
    if(password.trim().length === 0 || password.trim().length < 6 || !regexPassword.test(password.trim())){
        return false
    }
    return true
}

export const isValidEmail = (email) => {
    const regexEmail = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
    if(email.trim().length === 0 || !regexEmail.test(email.trim())){
        return false
    }
    return true
}