import AsyncStorage from "@react-native-community/async-storage"

export const setToken = async (token) => {
    try {
        await AsyncStorage.setItem("token", token)
        return true
    } catch (err) {
        return false
    }

}

export const getToken = async () => {
    try {
        const currentToken = await AsyncStorage.getItem('token')
            .then(v => v)
            .catch(err => {
                console.log(err)
                return null
            })
        return currentToken
    } catch (e) {
        console.error(`Exception in getToken => ${e}`)
        return null
    }

}

export const getUser = async () => {
    try {
        const currentUserData = await AsyncStorage.getItem('user')
            .then(v => JSON.parse(v))
            .catch(err => {
                console.log(err)
                return null
            })
        return currentUserData
    } catch (e) {
        console.error(`Exception in asycnstorage getUser => ${e}`)
        return null
    }

}


export const removeToken = async () => {
    try {
        await AsyncStorage.removeItem('token');
        return true
    } catch (e) {
        console.error(`Exception in removeToken => ${e}`)
        return false
    }

}

export const saveUserData = async (user) => {
    try {
        await AsyncStorage.setItem('user', JSON.stringify(user))
        return true
    } catch (e) {
        console.error(`Exception in saveUserData => ${e}`)
        return false
    }

}

export const removeUser = async () => {
    try {
        await AsyncStorage.removeItem('user');
        return true
    } catch (e) {
        console.error(`Exception in removeUser => ${e}`)
        return false
    }

}