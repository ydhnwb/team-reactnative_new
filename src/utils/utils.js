export const imagePatcher = (imageUrl) => {
    if(imageUrl.startsWith("http")){
        return imageUrl
    }
    return `${MOVIE_ENDPOINT}${imageUrl}`
}

export const currencyFormat = (num) => {
    return 'IDR ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
};

export const getProgress = (raised, goal) => {
    const progress = (raised / goal)
    if(progress> 1){
        return 1
    }
    return progress
}

export const dayDiffer2 = (dateStr) => {
    if(dateStr == null){
        return "~"
    }
    const date = dateStr.split("T")[0];
    const currentDate = (new Date()).toISOString().split('T')[0]
    const Difference_In_Time = new Date(currentDate).getTime() - (new Date(date)).getTime(); 
    const Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
    return Difference_In_Days 
}

export const dayDiffer = (dateStr) => {
    if(dateStr == null){
        return "~"
    }
    const date = dateStr.split("T")[0];
    const currentDate = (new Date()).toISOString().split('T')[0]
    const Difference_In_Time = (new Date(date)).getTime() - new Date(currentDate).getTime(); 
    const Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
    return Difference_In_Days < 0 ? 0 : Difference_In_Days
}

export const clearDots = (goal) => {
    let result = "";
    for(let i = 0; i<goal.length; i++){
        if(goal[i] != '.'){
            result += goal[i]
        }
    }
    return result
}

export const isoDateToMoment = (dateStr) => {
    const months = ["JAN", "FEB", "MAR","APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];

    const date = dateStr.split("T")[0];
    const currentDate = (new Date()).toISOString().split('T')[0]
    const Difference_In_Time = (new Date(date)).getTime() - new Date(currentDate).getTime(); 
    const Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);  
    if(Difference_In_Days == 0){
        return "Today"
    }else if(Difference_In_Days == -1){
        return "Yesterday"
    }
    const aDate = (new Date(date))
    return `${aDate.getDay()} ${months[aDate.getMonth()]} ${aDate.getFullYear()}`
}

export const formatRupiah = (angka, prefix = undefined) => {
    var number_string = angka.replace(/[^,\d]/g, '').toString(),
    split   		= number_string.split(','),
    sisa     		= split[0].length % 3,
    rupiah     		= split[0].substr(0, sisa),
    ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if(ribuan){
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? prefix+ " " + rupiah : '');
}