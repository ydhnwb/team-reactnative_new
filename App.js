import React, { useEffect } from 'react';
import { StatusBar } from 'react-native';
import { MainPage } from './src/pages/main/MainPage';
import { LoginPage } from './src/pages/login/LoginPage';
import { RegisterPage } from './src/pages/register/RegisterPage';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { AppBar } from './src/components/AppBar';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { DefaultTheme, Provider } from 'react-native-paper';
import * as res from './src/values/colors';
import { MyCampaignPage } from './src/pages/my_campaign/MyCampaignPage';
import { MyDonationPage } from './src/pages/my_donation/MyDonationPage';
import { EditProfilePage } from './src/pages/edit_profile/EditProfilePage';
import { UpdateCampaignProgressPage } from './src/pages/update_campaign_progress/UpdateCampaignProgressPage';
import { FundraiseDetailPage } from './src/pages/fundraise/FundraiseDetailPage';
import { DonationPage } from './src/pages/donate/DonationPage';
import ExplorePage from './src/pages/explore/ExplorePage';
import FilterAndSortScreen from './src/pages/filter_and_sort/FilterAndSortScreen';
import { SearchResultPage } from './src/pages/search_result/SearchResultPage';
import { Provider as ReduxProvider } from 'react-redux';
import { createStore } from 'redux';
import { PackedReducer } from './src/reducer';
import * as storage from './src/utils/SharedPref';
import * as actions from './src/utils/actions';
import { EditCampaignPage } from './src/pages/edit_campaign/EditCampaignPage';
import { CampaignProgressPage } from './src/pages/campaign_progress/CampaignProgressPage';
import { CampaignDonationPage } from './src/pages/campaign_donation/CampaignDonationPage';
import { CommentPage } from './src/pages/comment_page/CommentPage';
import { ByCategoryPage } from './src/pages/by_category/ByCategoryPage';
import SplashScreen from './src/pages/splash/SplashScreen';
import AboutPage from './src/pages/about/AboutPage';
import FaqPage from './src/pages/faq/FaqPage';
import ContactPage from './src/pages/contact_us/ContactPage';

const Stack = createStackNavigator();

const theme = {
  ...DefaultTheme,
  roundness: 2,

  colors: {
    ...DefaultTheme.colors,
    primary: res.PRIMARY_COLOR,
    accent: res.PRIMARY_BUTTON_COLOR,
  },
};

const App = () => {
  const headerStyle = {
    headerTintColor: res.PRIMARY_COLOR,
    headerTitleAlign: 'center',
    headerTitleStyle: {
      color: 'black',
      fontSize: 18
    }
  }

  const configureStore = createStore(PackedReducer)


  return (
    <ReduxProvider store={configureStore}>
      <Provider theme={theme}>
        <SafeAreaProvider >

          <StatusBar barStyle="light-content" />
          <NavigationContainer>
            <Stack.Navigator initialRouteName="Splash" >
              <Stack.Screen name="Splash" component={SplashScreen} options={{ headerShown: false }}/>
              <Stack.Screen name="Main" component={MainPage} options={{
                header: props => <AppBar {...props} />
              }} />
              <Stack.Screen name="Login" component={LoginPage} options={{ headerShown: false }} />
              <Stack.Screen name="Register" component={RegisterPage} options={{ headerShown: false }} />
              <Stack.Screen name="My campaigns" component={MyCampaignPage} options={headerStyle} />
              <Stack.Screen name="My donations" component={MyDonationPage} options={headerStyle} />
              <Stack.Screen name="Edit profile" component={EditProfilePage} options={headerStyle} />
              <Stack.Screen name="Fundraiser" component={FundraiseDetailPage} options={{ header: props => <AppBar {...props} /> }} />
              <Stack.Screen name="Update campaign progress" component={UpdateCampaignProgressPage} options={headerStyle} />
              <Stack.Screen name="Donation" component={DonationPage} options={headerStyle} />
              <Stack.Screen name="Filter and sort" component={FilterAndSortScreen} options={headerStyle} />
              <Stack.Screen name="Explore" component={ExplorePage} options={headerStyle} />
              <Stack.Screen name="Search result" component={SearchResultPage} options={headerStyle} />
              <Stack.Screen name="Edit campaign" component={EditCampaignPage} options={headerStyle}/>
              <Stack.Screen name="Campaign progress" component={CampaignProgressPage} options={headerStyle}/>
              <Stack.Screen name="Donations" component={CampaignDonationPage} options={headerStyle}/>
              <Stack.Screen name="Comments" component={CommentPage} options={headerStyle}/>
              <Stack.Screen name="By category" component={ByCategoryPage} options={headerStyle}/>
              <Stack.Screen name="About" component={AboutPage} options={headerStyle}/>
              <Stack.Screen name="FAQ" component={FaqPage} options={headerStyle}/>
              <Stack.Screen name="Contact" component={ContactPage} options={headerStyle}/>
            </Stack.Navigator>
          </NavigationContainer>
        
        </SafeAreaProvider >
      </Provider>
      </ReduxProvider>


  );
};


export default App;
