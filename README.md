# Team Tali Kasih [React Native]
This project is part of Tali Kasih App with other developers:
- Backend Team:
Werren, Hanif, Yonathan and Fanny
- Frontend Team:
Samudera, Gilbert, Tsaniya and Bagas
- React Native
Alif Hanamifiya and Prieyudha

# About this project
Tali kasih is a donation platform to helps you achieves your campaign.  
We are believe in crowdfunding, so we helps you spread your campaign across Indonesia and make other possible to support with donation, sharing and commenting.

# Please read! [For Developer]
Please read this readme file first before you start coding.

## This starter project
This starter project already contains some basic library like:  
- React Navigation (Stack, Tab) and its supplement like: gesture handler, maskedview, etc
- Already installed redux
- Already installed axios
- Already installed react-native-paper
- Already installed AsyncStorage

## Branching
Please create your own branch like: fiya-dev or prieyudha-dev, also work per feature.  
The step is:  
- clone this repo
- git checkout develop
- git branch fiya-dev
- git checkout fiya-dev

For example: creating authentication like login.  
make sure you are on fiya-dev branch!

- git branch feature/auth
- git checkout feature/auth

start coding...
if its all good, push.

- git add .
- git commit -m "Adding auth feature"
- git push origin feature/auth

after create an auth feature then you want to create other feature, back to your fiya dev branch.

- git checkout fiya-dev
- git branch feature/profile
- git checkout feature/profile

start coding...
if ok, push againn with:

- git add .
- git commit -m "Add feature profile"
- git push origin feature/profile

please back to fiya-dev/prieyudha-dev if you want to create other feature

## Coding style and guide
Here are some guide you must follow!
- Please use meaningful/proper variable name  
    for example, DONT USE THIS!:
    ```javascript
    const x = .....
    const n = .....
    ```
    Use like this:
    ```javascript
    const temporaryIndex = ...
    const filteredIndex = ....
    ```
- We are gonna separate the service file
    **Dont** put your web request code inside a react component like below:
    ```javascript
    export function LoginPage () {
        const doLogin = async () {
            await axios.post("url", payload, header....)
        }
    }
    ```
    Please create a service file for example, AuthService.js
    ```javascript
    export const doLogin = async (email, password) => {
        try{
            const response = await axios.post("url", ...)
            return {
                status_code: response.status,
                message....
            }
        }catch(exception){
            console.log(exception)
            return { status_code: null, message: e.toString() }
        }
    }
    
    //use that on component
    import * as AuthService from 'AuthService'
    
    export function LoginPage () {
        const doLogin = async () {
            const response = await AuthService.doLogin(..., ...)
            if(response.status_code === 200){
                //login success    
            }esle{
                //show error
            }
        }
    }
    ```
    do the same for AsyncStorage!
    
## Descriptive commit message
Please use descriptive commit message for example:  
- git commit -m "Fixed bugs async storage not saved when logged in"
- git commit -m "Add home page"
- git commit -m "Refactor profie page"

DONT DO THIS:
- git commit -m "r"
- git commit -m "test"
- git commit -m "push hehe"